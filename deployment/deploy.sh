#!/bin/sh
SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")

echo "== Stopping grottle =="
cd $SCRIPT_DIR
docker-compose -p 'grottle' down
    
if [ "$#" -eq 1 ]; then
    exit 0
fi

echo "== Building grottle =="
cd $SCRIPT_DIR/..
gradle :grottle-accountmanager:build
gradle :grottle-game:build
gradle :grottle-frontend:build

echo "== Copying grottle jar files =="
for FILE in $SCRIPT_DIR/../**/build/libs/*.jar; do
    JARNAME=$(basename $FILE | sed 's/\-[0-9].*/.jar/')
    cp -u $FILE $SCRIPT_DIR/$JARNAME
    echo $JARNAME
done

echo "== Deploying grottle =="
cd $SCRIPT_DIR
docker-compose -p 'grottle' up -d