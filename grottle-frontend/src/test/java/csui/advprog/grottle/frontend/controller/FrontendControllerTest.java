package csui.advprog.grottle.frontend.controller;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import csui.advprog.grottle.frontend.config.GrottleConfig;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.HashMap;
import java.util.Map;

@WebMvcTest(controllers = FrontendController.class)
class FrontendControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private GrottleConfig grottleConfig;

    @ParameterizedTest
    @ValueSource(strings = {"/", "/login/", "/register/", "/leaderboard/", "/logout/", "/game/"})
    void testControllerLinkNotLoginCorrect(String arg) throws Exception {
        Map<String, String> host = new HashMap<>();
        host.put("accountmanager", "1");
        host.put("game", "1");
        host.put("assets", "1");

        when(grottleConfig.getHost()).thenReturn(host);
        mvc.perform(get(arg))
            .andExpect(status().isOk());
    }
}
