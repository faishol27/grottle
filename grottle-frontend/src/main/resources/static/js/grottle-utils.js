function getCookie(cname) {
  let name = cname + "="
  let decodedCookie = decodeURIComponent(document.cookie)
  let ca = decodedCookie.split(';')
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ')
      c = c.substring(1)
    if (c.indexOf(name) === 0)
      return c.substring(name.length, c.length)
  }
  return ""
}
function setCookie(key, val) {
  var d = new Date();
  d.setTime(d.getTime() + 24*3600000);
  var expires = d.toUTCString();
  document.cookie = `${key}=${val}; expires=${expires}`
}
function getJwtClaim(token) {
  if (token === "") return null
  let payload = atob(token.split(".")[1])
  let jsonPayload = JSON.parse(payload)
  let timeNow = Math.floor(Date.now() / 1000)
  if (parseInt(jsonPayload['exp']) < timeNow) return null
  return jsonPayload
}
function isLoggedIn() {
  let token = getCookie("jwttoken")
  let claim = getJwtClaim(token)
  let timeNow = Math.floor(Date.now() / 1000)
  return claim !== null && parseInt(claim['exp']) >= timeNow
}
function createToast(mode, title, msg) {
  let toastHtml = `<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="toast-header bg-${mode} text-white">
        <strong class="me-auto">${title}</strong>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
      <div class="toast-body">${msg}</div>
    </div>`
  $('#toast-container').append(toastHtml)
  let toastElList = document.getElementsByClassName('toast')
  let toastEl = toastElList[toastElList.length - 1]
  let toast = new bootstrap.Toast(toastEl)
  toastEl.addEventListener('hidden.bs.toast', function () {
    $(this).remove()
  })
  toast.show()
}
function loadingButton(id, isLoad, val) {
  let html = val
  let oldHtml = $(`#${id}`).html()
  if (val === undefined) {
    html = "<div class='spinner-border text-light'></div>"
  }
  $(`#${id}`).prop("disable", isLoad)
  $(`#${id}`).html(html)
  if (isLoad) return oldHtml
}