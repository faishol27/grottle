package csui.advprog.grottle.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import csui.advprog.grottle.frontend.config.GrottleConfig;

@Controller
public class FrontendController {
    private String grottleHostStr = "grottleHost";
    @Autowired
    GrottleConfig grottleConf;

    @GetMapping(value = "/")
    public String homePage(Model model){
        model.addAttribute(grottleHostStr, grottleConf.getHost());
        return "home";
    }

    @GetMapping(value = "/login")
    public String loginPage(Model model){
        model.addAttribute(grottleHostStr, grottleConf.getHost());
        return "login";
    }

    @GetMapping(value = "/register")
    public String registerPage(Model model){
        model.addAttribute(grottleHostStr, grottleConf.getHost());
        return "register";
    }

    @GetMapping(value = "/logout")
    public String logoutPage(Model model){
        return "logout";
    }

    @GetMapping(value = "/leaderboard")
    public String leaderboardPage(Model model){
        model.addAttribute(grottleHostStr, grottleConf.getHost());
        return "leaderboard";
    }

    @GetMapping(value = "/game")
    public String gamePage(Model model){
        model.addAttribute(grottleHostStr, grottleConf.getHost());
        return "game";
    }
}
