package csui.advprog.grottle.frontend.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "grottle")
public class GrottleConfig {
    private Map<String, String> host;
    
    public void setHost(Map<String, String> host) {
        this.host = host;
    }

    public Map<String, String> getHost() {
        return this.host;
    }
}
