# Grottle

[![pipeline status](https://gitlab.com/faishol27/grottle/badges/master/pipeline.svg)](https://gitlab.com/faishol27/grottle/-/commits/master)
[![coverage report](https://gitlab.com/faishol27/grottle/badges/master/coverage.svg)](https://gitlab.com/faishol27/grottle/-/commits/master)

Final Projects - CSCM602023 Advanced Programming Term 2 2020/2021 @ Faculty of Computer Science, Universitas Indonesia

## Background Story
Grottle is a simple text-based turn role-playing game. In this game, you have to help our protagonist stay alive in the unknown dungeon. Living in this dungeon is not easy because you have to defend yourself from the monster. With a minimal item that he had, could you help him survive?

## About
Grottle is built in Java using Springboot. This project implements best practices in software development, such as clean code and design patterns, to achieve a great result.
Grottle is a microservice project that uses Docker for deployment. 

## Members
1. 1906285573 - Muhammad Faishol Amirul M. 
2. 1906350534 - Lucky Susanto 
3. 1906398414 - Rafli Bangsawan 
4. 1906398276 - Billy Vande Yohannes 

## Task Distribution
1. Muhammad Faishol A. M.
    - Login and Register Features
    - Game Features - Player Component
2. Lucky Susanto
    - Game Features - Monster Component
    - Game Features - Room Component
3. Rafli Bangsawan
    - Leaderboard Features
    - Game Features - Equipment Component
4. Billy Vande Yohannes
    - Game Features - Skill Component
    - Game Features - Item Component

## Link
* Account manager service: [http://grottle.faishol.net:1234/](http://grottle.faishol.net:1234/)
* Game service: [http://grottle.faishol.net:1235/](http://grottle.faishol.net:1235/)
* Frontend service: [http://grottle.faishol.net:1236/](http://grottle.faishol.net:1236/)
* Assets service: [http://grottle.faishol.net:1237/](http://grottle.faishol.net:1237/)
