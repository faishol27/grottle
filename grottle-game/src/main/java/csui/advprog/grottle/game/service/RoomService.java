package csui.advprog.grottle.game.service;

import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.*;
import csui.advprog.grottle.game.core.equipment.Armor;
import csui.advprog.grottle.game.core.equipment.Weapon;
import csui.advprog.grottle.game.core.item.Item;
import csui.advprog.grottle.game.core.skill.Skill;

import java.util.List;

public interface RoomService {
    public Monster getMonsterEncounter();
    public Monster getBossEncounter();
    public List<Item> getItemLoot(Monster monster);
    public List<Skill> getMonsterSkills(Monster monster);
    public Weapon getWeaponDrop();
    public Armor getArmorDrop();
    public List<Skill> getSkillList();
    public List<Item> getItemList();
    public Player playerRecover(Player player);
    public int generateRandomNum(int maxi);
}
