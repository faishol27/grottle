package csui.advprog.grottle.game.core.equipment;

public class HeavyArmor implements Armor {
    private int hpStat = 50;
    private int defenseStat = 10;

    @Override
    public int getHpStat() {
        return this.hpStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }

}
