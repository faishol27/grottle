package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class XPotion extends Item {

    public XPotion() {
        super("X-Potion", "Recover HP (L), Raise MAX HP(S) permanently");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setHpStat(Math.min(player.getHpStat() + 800, player.getMaxHpStat()));
        basePlayer.setMaxHpStat(basePlayer.getMaxHpStat() + 100);
    }
}
