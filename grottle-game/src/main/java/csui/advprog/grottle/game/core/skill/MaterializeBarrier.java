package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class MaterializeBarrier extends Skill {

    private static final int MP_COST = 20;

    public int getMpCost() {
        return 20;
    }

    public MaterializeBarrier() {
        super("Materialize: Barrier", "Raise DEF(M)");
    }

    public MaterializeBarrier(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setDefStat((int) (attacker.getDefStat() + 10 + 0.25 * skillLevel));
    }
}
