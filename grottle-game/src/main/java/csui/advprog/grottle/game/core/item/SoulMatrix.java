package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class SoulMatrix extends Item{

    public SoulMatrix() {
        super("Soul Matrix", "Deal 20% of Monster HP to monster");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        monster.setHpStat(monster.getHpStat() - monster.getHpStat() / 5);
    }
}
