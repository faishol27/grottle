package csui.advprog.grottle.game.core;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class JWTGenerator {
    private Key jwtKey;
    private static final Long EXP_TIME = 1000L*3600*24;
    private static JWTGenerator instance = null;

    private JWTGenerator() {
        String tmpKey = System.getProperty("grottle.jwtkey", "q3t6w9z$C&F)J@vcQfTjWnZr4u7x!A%D*G-TaPdSgUkXp2s5v8y/B?s(H+MbQeTh");
        jwtKey = Keys.hmacShaKeyFor(tmpKey.getBytes());
    }

    public static JWTGenerator getInstance() {
        if (instance == null) {
            instance = new JWTGenerator();
        }
        return instance;
    }

    public String getToken(Map<String, Object> claims) {
        var exp = new Date(System.currentTimeMillis() + EXP_TIME);
        JwtBuilder builder = Jwts.builder();
        for (Entry<String, Object> entry : claims.entrySet()) {
            builder.claim(entry.getKey(), entry.getValue());
        }
        return builder.signWith(jwtKey, SignatureAlgorithm.HS512).setExpiration(exp).compact();
    }

    public Map<String, Object> getClaim(String token) {
        try {
            Claims claim = Jwts.parserBuilder()
                    .setSigningKey(jwtKey).build()
                    .parseClaimsJws(token)
                    .getBody();
            return new HashMap<>(claim);
        } catch(Exception e) {
            return null;
        }
    }
}
