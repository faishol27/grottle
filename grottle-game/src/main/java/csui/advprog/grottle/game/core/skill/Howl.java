package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class Howl extends Skill {

    private static final int MP_COST = 20;

    public int getMpCost() {
        return 20;
    }

    public Howl() {
        super("Howl", "Lower Target's DEF(S), Lower Target's ATK(S)");
    }

    public Howl(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.setAtkStat((int) (target.getAtkStat() - 8 + 0.1 * skillLevel));
        target.setDefStat((int) (target.getDefStat() - 8 + 0.1 * skillLevel));
    }
}
