package csui.advprog.grottle.game.service;

import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.*;
import csui.advprog.grottle.game.core.equipment.Armor;
import csui.advprog.grottle.game.core.equipment.Weapon;
import csui.advprog.grottle.game.core.item.*;
import csui.advprog.grottle.game.core.skill.*;
import csui.advprog.grottle.game.core.equipment.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class RoomServiceImpl implements RoomService{
    private List<Monster> monsters = new ArrayList<>();
    private List<Monster> boss = new ArrayList<>();
    private boolean generateEntity = false;
    private List<Weapon> weaponLootTable = new ArrayList<>();
    private List<Armor> armorLootTable = new ArrayList<>();
    private List<Item> itemLootTable = new ArrayList<>();
    private boolean generateLootTable = false;
    private List<Skill> skillList = new ArrayList<>();
    private boolean generateSkillList = false;

    @Override
    public Monster getMonsterEncounter() {
        if (!generateEntity) {
            generateMonsterList();
            generateEntity = true;
        }
        return monsters.get(generateRandomNum(monsters.size()));
    }

    @Override
    public Monster getBossEncounter() {
        if (!generateEntity) {
            generateMonsterList();
            generateEntity = true;
        }
        return boss.get(generateRandomNum(boss.size()));
    }

    @Override
    public List<Item> getItemLoot(Monster monster) {
        if (!generateLootTable){
            generateLoots();
            generateLootTable = true;
        }

        List<Item> itemLootDrop = new ArrayList<>();
        int numItemDrop = generateRandomNum(4);

        if (monster.isBoss()) {
            itemLootDrop.add(new SoulFragment());
        }
        for (var i = 0; i < numItemDrop; i++) {
            itemLootDrop.add(itemLootTable.get(generateRandomNum(itemLootTable.size())));
        }
        return itemLootDrop;
    }

    @Override
    public List<Skill> getMonsterSkills(Monster monster) {
        return monster.getSkillList();
    }

    @Override
    public Weapon getWeaponDrop() {
        int rng = generateRandomNum(101);
        if (rng > 95) {
            if (!weaponLootTable.isEmpty()) {
                var weaponDrop = weaponLootTable.get(0);
                weaponLootTable.remove(0);
                return weaponDrop;
            } else {
                generateLoots();
                var weaponDrop = weaponLootTable.get(0);
                weaponLootTable.remove(0);
                return weaponDrop;
            }
        }
        return null;
    }

    @Override
    public Armor getArmorDrop() {
        int rng = generateRandomNum(101);
        if (rng > 95) {
            if (!armorLootTable.isEmpty()) {
                var armorDrop = armorLootTable.get(0);
                armorLootTable.remove(0);
                return armorDrop;
            } else {
                generateLoots();
                var armorDrop = armorLootTable.get(0);
                armorLootTable.remove(0);
                return armorDrop;
            }
        }
        return null;
    }

    @Override
    public Player playerRecover(Player player) {
        player.setAtkStat(player.getAtkStat() + 1);
        player.setDefStat(player.getDefStat() + 1);
        player.setMaxHpStat(player.getMaxHpStat() + 5);
        player.setHpStat(player.getHpStat() + 5);
        player.setMaxMpStat(player.getMaxMpStat() + 2);
        player.setMpStat(player.getMpStat() + 2);

        int hpRecover = player.getMaxHpStat() / 10;
        int mpRecover = player.getMaxMpStat() / 20;

        if (player.getHpStat() + hpRecover > player.getMaxHpStat()) {
            player.setHpStat(player.getMaxHpStat());
        } else {
            player.setHpStat(player.getHpStat() + hpRecover);
        }

        if (player.getMpStat() + mpRecover > player.getMaxMpStat()) {
            player.setMpStat(player.getMaxMpStat());
        } else {
            player.setMpStat(player.getMpStat() + mpRecover);
        }
        return player;
    }

    public int generateRandomNum(int max) {
        return ThreadLocalRandom.current().nextInt(0, max);
    }

    public void generateMonsterList() {
        if (monsters.isEmpty()) {
            monsters.add(new Landfish("Landfish", 100, 20, 3, 3));
            monsters.add(new Wildsnake("Wildsnake", 120, 16, 4, 2));
        }
        if (boss.isEmpty()) {
            boss.add(new ArmoredSoul("ArmoredSoul", 2000, 300, 50, 24));
            boss.add(new DruidOfTheTree("DruidOfTheTree", 1800, 600, 40, -20));
            boss.add(new IceTitan("IceTitan", 2400, 120, 30, 30));
            boss.add(new OmegaMachine("OmegaMachine", 2000, 400, 75, 12));
            boss.add(new Pandora("Pandora", 2666, 666, 66, 66));
        }
    }

    public void generateLoots() {
        weaponLootTable = new ArrayList<>();
        weaponLootTable.add(new Axe());
        weaponLootTable.add(new Bow());
        weaponLootTable.add(new Dagger());
        weaponLootTable.add(new Greatsword());
        weaponLootTable.add(new Longsword());

        armorLootTable = new ArrayList<>();
        armorLootTable.add(new HeavyArmor());
        armorLootTable.add(new LightArmor());
        armorLootTable.add(new Robe());

        itemLootTable = new ArrayList<>();
        itemLootTable.add(new AcidicLiquid());
        itemLootTable.add(new ArcaneMatrix());
        itemLootTable.add(new AresMight());
        itemLootTable.add(new BombShell());
        itemLootTable.add(new DemonTail());
        itemLootTable.add(new Elixir());
        itemLootTable.add(new Ether());
        itemLootTable.add(new HiEther());
        itemLootTable.add(new HiPotion());
        itemLootTable.add(new Potion());
        itemLootTable.add(new SoteriasTear());
        itemLootTable.add(new SoulMatrix());
        itemLootTable.add(new XEther());
        itemLootTable.add(new XPotion());
    }

    public void generateSkills() {
        skillList = new ArrayList<>();
        skillList.add(new AdrenalineRush());
        skillList.add(new AncientDischarge());
        skillList.add(new BadAura());
        skillList.add(new Blizzard());
        skillList.add(new DrainStrike());
        skillList.add(new Enroot());
        skillList.add(new Frenzy());
        skillList.add(new FullminatingDarkness());
        skillList.add(new HardSlash());
        skillList.add(new HazardForm());
        skillList.add(new HeavenWrath());
        skillList.add(new Howl());
        skillList.add(new IronNeedle());
        skillList.add(new ManaSiphon());
        skillList.add(new MaterializeArmiger());
        skillList.add(new MaterializeBarrier());
        skillList.add(new MaterializeWeapon());
        skillList.add(new MechaRenew());
        skillList.add(new MendWound());
        skillList.add(new NatureBlessing());
        skillList.add(new NatureWrath());
        skillList.add(new NormalAttack());
        skillList.add(new Recharge());
        skillList.add(new RecklessFlurry());
        skillList.add(new ShatterHeart());
        skillList.add(new VenomBite());
        skillList.add(new VolcanoEruption());

    }

    public List<Skill> getSkillList() {
        if (!generateSkillList) {
            generateSkills();
            generateSkillList = true;
        }
        return skillList;
    }

    public List<Item> getItemList() {
        if (!generateLootTable) {
            generateLoots();
            generateLootTable = true;
        }
        return itemLootTable;
    }

    public int skillIndexOf(String s){
        for (var i = 0; i < skillList.size(); i++) {
            if (skillList.get(i).getName().equals(s)) {
                return i;
            }
        }
        return -1;
    }

    public int itemIndexOf(String s){
        for (var i = 0; i < itemLootTable.size(); i++) {
            if (itemLootTable.get(i).getName().equals(s)) {
                return i;
            }
        }
        return -1;
    }


}
