package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class SoulFragment extends Item{

    public SoulFragment() {
        super("Soul Fragment",
                "Raise MAX HP(M), MAX MP(M), ATK(M), DEF(M) permanently");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setMaxMpStat(player.getMaxMpStat() + 30);
        player.setMaxHpStat(player.getMaxHpStat() + 120);
        player.setAtkStat(player.getAtkStat() + 12);
        player.setDefStat(player.getDefStat() + 12);

        basePlayer.setMaxMpStat(basePlayer.getMaxMpStat() + 30);
        basePlayer.setMaxHpStat(basePlayer.getMaxHpStat() + 120);
        basePlayer.setAtkStat(basePlayer.getAtkStat() + 12);
        basePlayer.setDefStat(basePlayer.getDefStat() + 12);
    }
}
