package csui.advprog.grottle.game.core.skill;


import csui.advprog.grottle.game.core.Creature;


public class AdrenalineRush extends Skill {

    public AdrenalineRush() {
        super("Adrenaline Rush","Recover HP(M), Raise ATK(M)");
    }

    public AdrenalineRush(String name, String description) {
        super(name, description);
    }
    private static final int MP_COST = 28;

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);

        attacker.setAtkStat((int) (attacker.getAtkStat() + 12 + 0.25 * skillLevel));

        attacker.setHpStat(Math.min(attacker.getHpStat() + 200, attacker.getMaxHpStat()));
    }

    public int getMpCost() {
        return 28;
    }
}
