package csui.advprog.grottle.game.core.ai;

import com.fasterxml.jackson.annotation.JsonProperty;
import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.skill.Skill;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.max;

public abstract class Monster implements MonsterI, Creature {
    @JsonProperty
    private String name;

    @JsonProperty
    private int atkStat;

    @JsonProperty
    private int defStat;

    @JsonProperty
    private int hpStat;

    @JsonProperty
    private int mpStat;

    @JsonProperty
    private int maxHpStat;

    @JsonProperty
    private int maxMpStat;

    @JsonProperty
    private int ragePoint;

    @JsonProperty
    private int turnCount;

    @JsonProperty
    private List<String> turnOrder;

    @JsonProperty
    private boolean boss;

    @JsonProperty
    private List<Skill> skillList;
    
    @JsonProperty
    private String rageDescription;

    public Monster() {

    }

    public Monster(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat, boolean boss) {
        this.name = name;
        this.maxHpStat = maxHpStat;
        this.hpStat = maxHpStat;
        this.maxMpStat = maxMpStat;
        this.mpStat = maxMpStat;
        this.atkStat = atkStat;
        this.defStat = defStat;

        this.turnCount = 0;
        this.ragePoint = generateRagePoint();
        this.turnOrder = generateTurnOrder();
        this.skillList = generateSkillList();

        this.boss = boss;
        this.rageDescription = generateRageDescription();
    }

    public void takeTurn(Player player) {
        recover();
        if (hpStat <= ragePoint) {
            rage();
        }
        List<String> action = Arrays.asList(turnOrder.get(turnCount % turnOrder.size()).split("-"));
        if (action.get(0).equals("Attack")) {
            player.takeDamage(this.attack());
        } else if (action.get(0).equals("Skill")) {
            var skill = skillList.get(getSkillIndex(action.get(1)));
            if (this.getMpStat() < skill.getMpCost()) {
                player.takeDamage(this.attack());
            } else {
                if (this.isBoss()) {
                    skill.execute(this, player, 50);
                } else {
                    skill.execute(this, player, 20);
                }
            }
        }
        turnCount += 1;

    }

    abstract void recover();

    public String getRageDescription() {
        return this.rageDescription;
    }

    public void setRageDescription(String rageDescription) {
        this.rageDescription = rageDescription;
    }

    public abstract String generateRageDescription();

    abstract List<String> generateTurnOrder();

    abstract List<Skill> generateSkillList();

    abstract int generateRagePoint();

    abstract int attack();

    public void takeDamage(int damage) {
        this.hpStat -= max(1, (damage - this.defStat) * 2 + turnCount * 3);
    }

    abstract void rage();

    public void setAtkStat(int atkStat) {
        this.atkStat = atkStat;
    }

    public void setDefStat(int defStat) {
        this.defStat = defStat;
    }

    public void setHpStat(int hpStat) {
        this.hpStat = hpStat;
    }

    public void setMpStat(int mpStat) {
        this.mpStat = mpStat;
    }

    public void setMaxHpStat(int maxHpStat) {
        this.maxHpStat = maxHpStat;
    }

    public void setMaxMpStat(int maxMpStat) {
        this.maxMpStat = maxMpStat;
    }

    public void setTurnCount(int turnCount) {
        this.turnCount = turnCount;
    }

    public void setRagePoint(int ragePoint) {
        this.ragePoint = ragePoint;
    }

    public void setTurnOrder(List<String> turnOrder) {
        this.turnOrder = turnOrder;
    }

    public void setBoss(boolean boss) {
        this.boss = boss;
    }

    public void setSkillList(List<Skill> skillList) {
        this.skillList = skillList;
    }

    public int getAtkStat() {
        return atkStat;
    }

    public int getDefStat() {
        return defStat;
    }

    public int getHpStat() {
        return hpStat;
    }

    public int getMaxHpStat() {
        return maxHpStat;
    }

    public int getMaxMpStat() {
        return maxMpStat;
    }

    public int getMpStat() {
        return mpStat;
    }

    public int getRagePoint() {
        return ragePoint;
    }

    public int getTurnCount() {
        return turnCount;
    }

    public List<String> getTurnOrder() {
        return turnOrder;
    }

    public String getName() {
        return name;
    }

    public boolean isBoss() {
        return boss;
    }

    public List<Skill> getSkillList() {
        return skillList;
    }

    public int getSkillIndex (String skillName) {
        for (var i = 0; i < skillList.size(); i++) {
            if (skillName.equals(skillList.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }
}
