package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class MaterializeWeapon extends Skill {

    private static final int MP_COST = 20;

    public int getMpCost() {
        return 20;
    }

    public MaterializeWeapon() {
        super("Materialize: Weapon", "Raise ATK(M)");
    }

    public MaterializeWeapon(String name, String description) {
        super(name, description);
    }


    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setAtkStat((int) (attacker.getAtkStat() + 12 + 0.25 * skillLevel));

    }
}
