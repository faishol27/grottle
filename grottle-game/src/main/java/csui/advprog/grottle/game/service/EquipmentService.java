package csui.advprog.grottle.game.service;

import csui.advprog.grottle.game.core.equipment.Weapon;
import csui.advprog.grottle.game.core.equipment.Armor;

public interface EquipmentService {
    Iterable<Weapon> getWeapons();
    Iterable<Armor> getArmors();
}
