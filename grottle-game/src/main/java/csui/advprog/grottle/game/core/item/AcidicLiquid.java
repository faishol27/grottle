package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class AcidicLiquid extends Item{

    public AcidicLiquid() {
        super("Acidic Liquid", "Deal Damage(M) to monster, Decrease ATK(L) to monster");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        monster.setHpStat(monster.getHpStat() - 100);
        monster.setAtkStat(monster.getAtkStat() - 120);
    }
}
