package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class MechaRenew extends Skill {

    private static final int MP_COST = 120;

    public int getMpCost() {
        return 120;
    }

    public MechaRenew() {
        super("Mecha: Renew", "Recover HP to Max");
    }

    public MechaRenew(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setHpStat(attacker.getMaxHpStat());

    }
}
