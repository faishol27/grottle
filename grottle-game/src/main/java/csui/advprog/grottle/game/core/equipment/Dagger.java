package csui.advprog.grottle.game.core.equipment;

public class Dagger implements Weapon {
    private int attackStat = 3;
    private int defenseStat = 1;

    @Override
    public int getAttackStat() {
        return this.attackStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }

}
