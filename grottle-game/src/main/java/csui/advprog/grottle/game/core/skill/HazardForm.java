package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class HazardForm extends Skill {

    private static final int MP_COST = 18;

    public int getMpCost() {
        return 18;
    }

    public HazardForm() {
        super("Hazard Form", "Damage Self(S), Raise ATK(M)");
    }

    public HazardForm(String name, String description) {
        super(name, description);
    }

    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.takeDamage(100 - skillLevel);
        attacker.setAtkStat((int) (attacker.getAtkStat() + 12 + 0.15 * skillLevel));
    }
}
