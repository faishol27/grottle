package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class BombShell extends Item {

    public BombShell() {
        super("Bomb Shell",
                "Deal Damage(S) to monster, Decrease DEF(M) to monster");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        monster.setHpStat(monster.getHpStat() - 100);
        monster.setDefStat(monster.getDefStat() - 30);
    }
}
