package csui.advprog.grottle.game.core.equipment;

public class Robe implements Armor {

    private int hpStat = 10;
    private int defenseStat = 3;

    @Override
    public int getHpStat() {
        return this.hpStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }

}