package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class AncientDischarge extends Skill {

    private static final int MP_COST = 28;

    public int getMpCost() {
        return 28;
    }

    public AncientDischarge() {
        super("Ancient Discharge",
                "Deal Damage(M) to self, Recover MP according to damage taken");
    }

    public AncientDischarge(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);

        attacker.setHpStat(attacker.getHpStat() - 250 + 4 * skillLevel);
        attacker.setMpStat(attacker.getMpStat() + 70 + skillLevel);
    }
}
