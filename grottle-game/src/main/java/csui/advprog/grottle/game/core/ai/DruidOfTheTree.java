package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class DruidOfTheTree extends Monster{
    public DruidOfTheTree(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, true);
    }

    public DruidOfTheTree() {
        super();
    }

    @Override
    void recover() {
        if (this.getHpStat() + this.getAtkStat() > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + this.getAtkStat());
        }
    }

    @Override
    public String generateRageDescription() {
        return "The Druid converts all its DEF into ATK!";
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new BadAura());
        skillList.add(new NatureBlessing());
        skillList.add(new VolcanoEruption());
        skillList.add(new NatureWrath());
        skillList.add(new AncientDischarge());
        skillList.add(new HazardForm());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Bad Aura");
        turnOrder.add("Skill-Hazard Form");
        turnOrder.add("Skill-Nature's Wrath");
        turnOrder.add("Skill-Nature's Wrath");
        turnOrder.add("Skill-Nature's Blessing");
        turnOrder.add("Skill-Nature's Blessing");
        turnOrder.add("Skill-Ancient Discharge");
        turnOrder.add("Attack");
        turnOrder.add("Attack");
        turnOrder.add("Skill-Volcano Eruption");
        return turnOrder;
    }

    @Override
    int generateRagePoint() {
        return this.getAtkStat();
    }

    @Override
    int attack() {
        this.setRagePoint(this.getRagePoint()+1000);
        return this.getAtkStat() + (int) (this.getHpStat() * 0.01);
    }

    @Override
    void rage() {
        this.setAtkStat(this.getAtkStat() + this.getDefStat());
        this.setDefStat(0);
        this.setRagePoint(0);
    }
}
