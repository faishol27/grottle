package csui.advprog.grottle.game.core.equipment;

public interface Armor extends Equipment {
    int getHpStat();
    int getDefenseStat();
}
