package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class NatureBlessing extends Skill {

    private static final int MP_COST = 40;

    public int getMpCost() {
        return 40;
    }

    public NatureBlessing() {
        super("Nature's Blessing", "Raise DEF(M), Recover HP(L)");
    }

    public NatureBlessing(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);

        attacker.setDefStat(attacker.getDefStat() + 30);

        attacker.setHpStat(Math.min(attacker.getHpStat() + 500, attacker.getMaxHpStat()));
    }
}
