package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class Potion extends Item {

    public Potion() {
        super("Potion", "Recover HP(S)");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setHpStat(Math.min(player.getHpStat() + 100, player.getMaxHpStat()));
    }
}
