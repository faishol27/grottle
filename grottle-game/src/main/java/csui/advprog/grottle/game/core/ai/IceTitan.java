package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class IceTitan extends Monster{
    public IceTitan(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, true);
    }

    public IceTitan() {
        super();
    }

    @Override
    void recover() {
        if (this.getHpStat() + this.getDefStat() > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + this.getDefStat());
        }
    }

    @Override
    public String generateRageDescription() {
        return "Icy shroud envelops the Titan! Increased DEF(S)!";
    }

    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new BadAura());
        skillList.add(new ShatterHeart());
        skillList.add(new Howl());
        skillList.add(new Blizzard());
        skillList.add(new Frenzy());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        final var howl = "Skill-Howl";
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Bad Aura");
        turnOrder.add(howl);
        turnOrder.add("Skill-Frenzy");
        turnOrder.add(howl);
        turnOrder.add("Skill-Blizzard");
        turnOrder.add(howl);
        turnOrder.add("Skill-Shatter Heart");
        turnOrder.add("Attack");
        turnOrder.add("Blizzard");
        turnOrder.add("Attack");
        return turnOrder;
    }

    @Override
    int generateRagePoint() {
        return this.getMaxHpStat();
    }

    @Override
    int attack() {
        this.setRagePoint(this.getMaxHpStat());
        return (int) (2.5 * this.getDefStat() + 0.2 * this.getAtkStat());
    }

    @Override
    void rage() {
        this.setRagePoint(0);
        this.setDefStat(this.getDefStat() + 7);
    }
}
