package csui.advprog.grottle.game.core.skill;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import csui.advprog.grottle.game.core.Creature;

@JsonDeserialize(using = SkillDeserializer.class)
public abstract class Skill {
    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    private static final double MULT_GROWTH = 0.05;

    public Skill(String name, String description){
        this.name = name;
        this.description = description;
    }

    public Skill() {

    }

    public abstract int getMpCost();

    public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSkillType() {
        return this.getClass().getName();
    }

    public double getMultGrowth() {
        return MULT_GROWTH;
    }

    public abstract void execute(Creature attacker, Creature target, int skillLevel);
}
