package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class SoteriasTear extends Item {

    public SoteriasTear() {
        super("Soteria's Tear",
                "Raise DEF(M) for 1 battle, Raise DEF(S) permanently");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setDefStat(player.getDefStat() + 55);

        basePlayer.setDefStat(basePlayer.getDefStat() + 5);
    }

}
