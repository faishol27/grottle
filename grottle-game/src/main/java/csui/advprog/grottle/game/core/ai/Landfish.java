package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class Landfish extends Monster{
    public Landfish(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, false);
    }

    public Landfish() {
        super();
    }

    @Override
    public void recover() {
        if (this.getHpStat() + 3 + this.getTurnCount()> this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + 3);
        }

        if (this.getMpStat() + 2 > this.getMaxMpStat()) {
            this.setMpStat(this.getMaxMpStat());
        } else {
            this.setMpStat(this.getMpStat() + 2 + this.getTurnCount());
        }
    }

    @Override
    public String generateRageDescription() {
        return "Landfish is enraged! It gets increased ATK(M) and increased recovery power!";
    }

    @Override
    int generateRagePoint() {
        return (int) (this.getMaxHpStat()/5.0 + 2);
    }

    @Override
    public int attack() {
        return (int) (this.getAtkStat() * 2.3);
    }

    @Override
    public void rage() {
        this.setRagePoint(0);
        recover();
        this.setAtkStat(this.getAtkStat() + 10);
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Blizzard());
        skillList.add(new Recharge());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Blizzard");
        turnOrder.add("Skill-Blizzard");
        turnOrder.add("Skill-Recharge");
        turnOrder.add("Attack");
        //"Item-{Nama Item}" : "Item-Acidic Liquid"
        return turnOrder;
    }
}
