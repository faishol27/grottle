package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class Frenzy extends Skill{

    private static final int MP_COST = 19;

    public int getMpCost() {
        return 19;
    }

    public Frenzy() {
        super("Frenzy", "Raise ATK(S), Deal Damage(M)");
    }

    public Frenzy(String name, String description) {
        super(name, description);
    }

    private int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.3 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setAtkStat(attacker.getAtkStat() + 6);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
    }
}
