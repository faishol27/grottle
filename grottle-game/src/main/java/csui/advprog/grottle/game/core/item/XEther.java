package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class XEther extends Item {

    public XEther() {
        super("X-Ether", "Recover MP(M), Raise MAX MP(S) permanently");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setMpStat(Math.min(player.getMpStat() + 100, player.getMaxMpStat()));
        basePlayer.setMaxMpStat(basePlayer.getMaxMpStat() + 20);
    }
}
