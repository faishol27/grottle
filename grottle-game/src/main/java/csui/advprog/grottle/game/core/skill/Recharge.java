package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class Recharge extends Skill {

    private static final int MP_COST = 0;

    public Recharge() {
        super("Recharge", "Recover MP(MIN)");
    }

    public Recharge(String name, String description) {
        super(name, description);
    }

    public int getMpCost() {
        return 0;
    }


    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setMpStat(Math.min(attacker.getMpStat() + 10 + skillLevel, attacker.getMaxMpStat()));
    }
}
