package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class VolcanoEruption extends Skill {

    private static final int MP_COST = 50;

    public int getMpCost() {
        return 50;
    }

    public VolcanoEruption() {
        super("Volcano Eruption", "Deal Damage(L), Lower Target's DEF(L)");
    }

    public VolcanoEruption(String name, String description) {
        super(name, description);
    }


    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.5 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
        target.setDefStat(target.getDefStat() - 80);

    }
}
