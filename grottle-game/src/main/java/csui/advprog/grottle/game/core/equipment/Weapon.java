package csui.advprog.grottle.game.core.equipment;

public interface Weapon extends Equipment {
    int getAttackStat();
    int getDefenseStat();
}
