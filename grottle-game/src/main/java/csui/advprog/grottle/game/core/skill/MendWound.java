package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class MendWound extends Skill {

    private static final int MP_COST = 12;

    public int getMpCost() {
        return 12;
    }

    public MendWound() {
        super("Mend Wound", "Recover HP(S), Raise DEF(M)");
    }

    public MendWound(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setDefStat((int) (attacker.getDefStat() + 6 + 0.275 * skillLevel));
        attacker.setHpStat(Math.min(attacker.getHpStat() + 75 + 3 * skillLevel, attacker.getMaxHpStat()));
    }
}
