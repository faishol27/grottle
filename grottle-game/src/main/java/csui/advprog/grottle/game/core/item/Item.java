package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public abstract class Item {
    private final String name;
    private final String description;

    protected Item(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public abstract void execute(Creature player, Creature monster, Creature basePlayer);
}
