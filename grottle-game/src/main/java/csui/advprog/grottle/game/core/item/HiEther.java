package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class HiEther extends Item {

    public HiEther() {
        super("Hi-Ether", "Recover MP(M)");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setMpStat(Math.min(player.getMpStat() + 45, player.getMaxMpStat()));
    }
}
