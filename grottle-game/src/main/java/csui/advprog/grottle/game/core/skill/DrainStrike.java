package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class DrainStrike extends Skill{

    private static final int MP_COST = 33;

    public int getMpCost() {
        return 33;
    }

    public DrainStrike() {
        super("Drain Strike","Deal Damage(L), recover HP according to damage");
    }

    public DrainStrike(String name, String description) {
        super(name, description);
    }

    private int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);

        int damage = dealDamage(attacker.getAtkStat(), skillLevel);

        target.takeDamage(damage);

        attacker.setHpStat(Math.min(attacker.getHpStat() + damage, attacker.getMaxHpStat()));
    }
}
