package csui.advprog.grottle.game.core.skill;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class SkillDeserializer extends StdDeserializer<Skill> {

    public SkillDeserializer() {
        this(null);
    }

    public SkillDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Skill deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String className = node.get("skillType").asText();
        String name = node.get("name").asText();
        String desc = node.get("description").asText();

        try {
            var cls = Class.forName(className);
            return (Skill) cls.getDeclaredConstructor(String.class, String.class).newInstance(name, desc);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            return null;
        }
    }
}
