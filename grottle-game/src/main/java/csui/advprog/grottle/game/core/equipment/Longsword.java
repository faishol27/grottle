package csui.advprog.grottle.game.core.equipment;

public class Longsword implements Weapon {
    private int attackStat = 5;
    private int defenseStat = 5;

    @Override
    public int getAttackStat() {
        return this.attackStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }
}
