package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class Pandora extends Monster{

    public Pandora(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, true);
    }

    public Pandora() {
        super();
    }

    @Override
    public void recover() {
        if (this.getHpStat() + (int) (1.7*this.getTurnCount()) > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + (int) (1.7*this.getTurnCount()));
        }

        if (this.getMpStat() + 30 > this.getMaxMpStat()) {
            this.setMpStat(this.getMaxMpStat());
        } else {
            this.setMpStat(this.getMpStat() + 30);
        }
    }

    @Override
    public String generateRageDescription() {
        return "Pandora is absorbing dead souls! Recovering HP(S)!";
    }

    @Override
    int generateRagePoint() {
        return this.getMaxHpStat();
    }

    @Override
    public int attack() {
        this.setRagePoint(this.getRagePoint() + (int) (this.getMpStat()*0.1));
        this.setMpStat(this.getMpStat() - 45);
        if (this.getMpStat() < 0) {
            this.setMpStat(0);
        }
        return (int) (this.getMpStat() * 0.15 + this.getAtkStat() * 0.95);
    }

    @Override
    void rage() {
        this.setHpStat(this.getHpStat() + this.getAtkStat());
        this.setRagePoint(this.getRagePoint() - this.getAtkStat() * 4);
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new BadAura());
        skillList.add(new FullminatingDarkness());
        skillList.add(new Howl());
        skillList.add(new DrainStrike());
        skillList.add(new ManaSiphon());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Bad Aura");
        turnOrder.add("Skill-Fullminating Darkness");
        turnOrder.add("Skill-Howl");
        turnOrder.add("Skill-Howl");
        turnOrder.add("Attack");
        turnOrder.add("Skill-Drain Strike");
        turnOrder.add("Skill-Mana Siphon");
        return turnOrder;
    }
}
