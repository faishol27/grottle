package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class HeavenWrath extends Skill {

    public HeavenWrath() {
        super("Heaven's Wrath", "Self MP Become 0, Deal Damage (MAX)");
    }

    public int getMpCost() {
        return 0;
    }

    public HeavenWrath(String name, String description) {
        super(name, description);
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(0);
        target.takeDamage(attacker.getAtkStat() * 5);
    }

}
