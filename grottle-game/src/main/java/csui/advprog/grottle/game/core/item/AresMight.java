package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class AresMight extends Item {

    public AresMight() {
        super("Ares Might",
                "Raise ATK(M) for 1 battle, Raise ATK(S) permanently");
    }

    private int increaseAttack(int atkStat) {
        return atkStat + 50;
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setAtkStat(increaseAttack(player.getAtkStat()));

        basePlayer.setAtkStat(basePlayer.getAtkStat() + 5);
        player.setAtkStat(player.getAtkStat() + 5);
    }
}