package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class Wildsnake extends Monster{

    public Wildsnake(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, false);
    }

    public Wildsnake() {
        super();
    }

    @Override
    public void recover() {
        if (this.getHpStat() + 4 > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + 4);
        }

        if (this.getMpStat() + 1 > this.getMaxMpStat()) {
            this.setMpStat(this.getMaxMpStat());
        } else {
            this.setMpStat(this.getMpStat() + 1);
        }
    }

    @Override
    public String generateRageDescription() {
        return "Wildsnake is distressed! It gain ATK(S)!";
    }

    @Override
    int generateRagePoint() {
        return (int) (this.getMaxHpStat()/8.0);
    }

    @Override
    public int attack() {
        return this.getAtkStat() * 3;
    }

    @Override
    public void rage() {
        this.setRagePoint(0);
        this.setAtkStat(this.getAtkStat() + 6);
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new VenomBite());
        skillList.add(new Recharge());
        skillList.add(new HardSlash());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Attack");
        turnOrder.add("Skill-Venom Bite");
        turnOrder.add("Skill-Recharge");
        turnOrder.add("Skill-Hard Slash");
        return turnOrder;
    }
}
