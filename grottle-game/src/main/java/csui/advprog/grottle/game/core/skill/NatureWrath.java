package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class NatureWrath extends Skill {
    private static final int MP_COST = 24;

    public NatureWrath() {
        super("Nature's Wrath", "Deal Damage(S), Deal MP Damage(S)");
    }

    public int getMpCost() {
        return 24;
    }

    public NatureWrath(String name, String description) {
        super(name, description);
    }

    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);

        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));

        int mpDamage = (int) (18 + 0.5 * skillLevel);

        target.setMpStat(Math.max(target.getMpStat() - mpDamage, 0));

    }
}
