package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class OmegaMachine extends Monster{
    public OmegaMachine(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, true);
    }

    public OmegaMachine() {
        super();
    }

    @Override
    void recover() {
        if ((this.getHpStat() + (int) (this.getMaxHpStat() / 30.0)) > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + (int) (this.getMaxHpStat() / 30.0));
        }

        if ((this.getMpStat() + (int) (this.getMaxMpStat() / 50.0)) > this.getMaxMpStat()) {
            this.setMpStat(this.getMaxMpStat());
        } else {
            this.setMpStat(this.getMpStat() + (int) (this.getMaxMpStat() / 50.0));
        }
        this.setRagePoint(this.getRagePoint() + this.generateRagePoint());
    }

    @Override
    public String generateRageDescription() {
        return "Omega Machine starts upgrading itself! Increase MAX HP(MIN)!";
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new BadAura());
        skillList.add(new MechaRenew());
        skillList.add(new MaterializeBarrier());
        skillList.add(new MaterializeWeapon());
        skillList.add(new HazardForm());
        skillList.add(new ManaSiphon());
        skillList.add(new Recharge());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Bad Aura");
        turnOrder.add("Skill-Materialize Barrier");
        turnOrder.add("Skill-Matereialize Weapon");
        turnOrder.add("Skill-Hazard Form");
        turnOrder.add("Skill-Recharge");
        turnOrder.add("Skill-Mana Siphon");
        turnOrder.add("Skill-Mecha Renew");
        return turnOrder;
    }

    @Override
    int generateRagePoint() {
        return (int) (this.getMaxHpStat() / 30.0);
    }

    @Override
    int attack() {
        return 333;
    }

    @Override
    void rage() {
        this.setMaxHpStat(this.getMaxHpStat() + (int) (this.getMaxHpStat() * 0.04));
        this.setRagePoint(0);
    }
}
