package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class HardSlash extends Skill {

    private static final int MP_COST = 6;

    public int getMpCost() {
        return 6;
    }

    public HardSlash() {
        super("Hard Slash", "Deal Damage(S)");
    }

    public HardSlash(String name, String description) {
        super(name, description);
    }

    private int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
    }
}
