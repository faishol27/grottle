package csui.advprog.grottle.game.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advprog.grottle.game.core.JWTGenerator;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.Monster;
import csui.advprog.grottle.game.core.item.Item;
import csui.advprog.grottle.game.core.skill.Skill;
import csui.advprog.grottle.game.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(path = "/game")
public class RoomController {
    @Autowired
    private RoomService roomService;
    private JWTGenerator jwtGen = JWTGenerator.getInstance();

    private static final String MONSTER = "monster";
    private static final String PLAYER = "player";
    private static final String BASEPLAYER = "basePlayer";
    private static final String MONSTERTYPE = "monsterType";
    private static final String HAS_ATTACK = "hasAttack";
    private static final String HAS_USEITEM = "hasUseItem";
    private static final String HAS_USESKILL = "hasUseSkill";
    private static final String DAMAGE = " damage";
    private static final String STATUS = "status";

    @PostMapping(path = "/encounter-monster", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> encounterNormalMonster(@RequestBody Map<String, String> bodyReq) {
        String tokenPlayer = bodyReq.get("data");
        Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
        var player = new ObjectMapper().convertValue(claim.get(PLAYER), Player.class);
        var monster = roomService.getMonsterEncounter();
        monster.setAtkStat((int) (player.getAtkStat() * 0.55 + 0.05 * player.getMaxHpStat()));
        monster.setDefStat((int) (player.getDefStat() * 0.55 + 0.05 * player.getMaxMpStat()));
        monster.setMaxHpStat((int) (player.getMaxHpStat() * 0.75 + 0.66 * player.getAtkStat()));
        monster.setHpStat(monster.getMaxHpStat());
        monster.setMaxMpStat((int) (player.getMaxMpStat() * 0.55 + 0.44 * player.getDefStat()));
        monster.setMpStat(monster.getMaxMpStat());
        if (player.getAtkStat() + player.getDefStat() >= 160) {
            int randNum = roomService.generateRandomNum(100);
            if (randNum > 85) {
                monster = roomService.getBossEncounter();
            }
        }

        claim.put(MONSTERTYPE, monster.getClass().getName());
        claim.put(MONSTER, monster);
        claim.put(BASEPLAYER, player);
        List<String> log = new ArrayList<>();
        log.add("######################################################################");
        log.add("You have encountered " + monster.getName());
        log.add("######################################################################");
        claim.put("log", log);
        claim.remove(STATUS);
        claim.put(HAS_ATTACK, false);
        claim.put(HAS_USEITEM, false);
        claim.put(HAS_USESKILL, false);
        return ResponseEntity.ok(jwtGen.getToken(claim));
    }

    @PostMapping(path = "/encounter-boss", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> encounterBossMonster(@RequestBody Map<String, String> bodyReq) {
        String tokenPlayer = bodyReq.get("data");
        Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
        var player = new ObjectMapper().convertValue(claim.get(PLAYER), Player.class);
        var monster = roomService.getBossEncounter();
        claim.put(MONSTERTYPE, monster.getClass().getName());
        claim.put(MONSTER, monster);
        claim.put(BASEPLAYER, player);
        List<String> log = new ArrayList<>();
        log.add("You feel an overwhelming presence!");
        log.add("You have encountered" + monster.getName());
        claim.put("log", log);
        claim.remove(STATUS);
        claim.put(HAS_ATTACK, false);
        claim.put(HAS_USEITEM, false);
        claim.put(HAS_USESKILL, false);
        return ResponseEntity.ok(jwtGen.getToken(claim));
    }

    @PostMapping(path="/battle/{action}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> playerAction(@PathVariable(value = "action") String action, @RequestBody Map<String, String> bodyReq)  throws JsonProcessingException, ClassNotFoundException {
        String tokenPlayer = bodyReq.get("data");
        Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
        var player = new ObjectMapper().convertValue(claim.get(PLAYER), Player.class);
        var basePlayer = new ObjectMapper().convertValue(claim.get(BASEPLAYER), Player.class);
        var monster = (Monster) new ObjectMapper().convertValue(claim.get(MONSTER), Class.forName(claim.get(MONSTERTYPE).toString()));
        List<String> log = new ArrayList<>();
        List<String> parsedAction = actionParse(action);

        int mTempHp = monster.getHpStat();

        if (parsedAction.get(0).equals("Attack")) {
            claim.put(HAS_ATTACK, true);
            player.doAttack(monster);
            log.add("You attacked " + monster.getName());
            log.add("You dealt " + (mTempHp - monster.getHpStat()) + DAMAGE);
        } else if (parsedAction.get(0).equals("Skill")) {
            List<Skill> skillList = roomService.getSkillList();
            var skill = skillNameToSkillObj(skillList, parsedAction.get(1));
            if (player.useSkill(parsedAction.get(1)) > 0 && player.getMpStat() >= skill.getMpCost()) {
                claim.put(HAS_USESKILL, true);
                skill.execute(player, monster, player.getSkillLevel(parsedAction.get(1)));
                log.add("You used Skill: " + skill.getName());
                log.add("Skill effect: " + skill.getDescription());
                log.add("You dealt " + (mTempHp - monster.getHpStat()) + DAMAGE);
            }
        } else if (parsedAction.get(0).equals("Item")) {
            if (player.useItem(parsedAction.get(1))) {
                claim.put(HAS_USEITEM, true);
                basePlayer.useItem(parsedAction.get(1));
                List<Item> itemList = roomService.getItemList();
                var item = itemNameToItemObj(itemList, parsedAction.get(1));
                item.execute(player, monster, basePlayer);
                log.add("You used Item: " + item.getName());
                log.add("Item effect: " + item.getDescription());
            }
        } else if (parsedAction.get(0).equals("EndTurn")) {
            int temp = player.getHpStat();
            claim.put(HAS_ATTACK, false);
            claim.put(HAS_USEITEM, false);
            claim.put(HAS_USESKILL, false);
            monster.takeTurn(player);
            log.add("Enemy turn");
            List<String> monsterAction = Arrays.asList(monster.getTurnOrder().get(monster.getTurnCount() % monster.getTurnOrder().size()).split("-", 2));
            log.add("The monster regenerate some hp and mp");
            if (monster.getHpStat() < monster.getRagePoint()) {
                log.add("The monster is enraged!");
                log.add(monster.getRageDescription());
            }
            if (monsterAction.get(0).equals("Attack")) {
                log.add("The monster attacked you!");
                log.add("It dealt " + (temp - player.getHpStat()) + DAMAGE);
            } else if (monsterAction.get(0).equals("Skill")) {
                List<Skill> skillList = roomService.getSkillList();
                var skill = skillNameToSkillObj(skillList, monsterAction.get(1));
                if (monster.getMpStat() < skill.getMpCost()) {
                    log.add("The monster attacked you!");
                    log.add("It dealt " + (temp - player.getHpStat()) + DAMAGE);
                } else {
                    log.add("The monster uses Skill: " + skill.getName());
                    log.add("Skill effect: " + skill.getDescription());
                    log.add("It dealt " + (temp - player.getHpStat()) + DAMAGE);
                }
            }


        }

        claim.put(STATUS, "battle");
        claim.put(MONSTER, monster);
        claim.put(PLAYER, player);
        claim.put(BASEPLAYER, basePlayer);

        if (gameEnd(player, monster) == -1) {
            claim.put(STATUS, "lose");
            claim.put(PLAYER, basePlayer);
            claim.remove(BASEPLAYER);
            claim.remove(MONSTER);
            log.add("GAME OVER!");
            return ResponseEntity.ok(jwtGen.getToken(claim));
        } else if (gameEnd(player, monster) == 1) {
            claim.remove(BASEPLAYER);
            claim.put(MONSTER, monster);
            claim.put(STATUS, "win");
            log.add("You are victorious!");

            basePlayer.setHpStat(player.getHpStat());
            basePlayer.setMpStat(player.getMpStat());

            log.add("You recover some HP and MP and increased stats!");
            basePlayer = roomService.playerRecover(basePlayer);

            List<Skill> skillReward = roomService.getMonsterSkills(monster);
            log.add("You gained the following skill(s): ");
            for (Skill s : skillReward) {
                basePlayer.addSkill(s.getName());
                log.add(s.getName());
            }
            List<Item> itemLoot = roomService.getItemLoot(monster);
            log.add("You gained the following item(s): ");
            for (Item i : itemLoot) {
                basePlayer.addItem(i.getName());
                log.add(i.getName());
            }

            var weaponDrop = roomService.getWeaponDrop();
            var armorDrop = roomService.getArmorDrop();

            if (!Objects.isNull(weaponDrop)) {
                basePlayer.addEquipment(weaponDrop.toString(), "weapon");
                log.add("You obtained the weapon: " + weaponDrop.getClass().getSimpleName());
            }
            if (!Objects.isNull(armorDrop)) {
                basePlayer.addEquipment(armorDrop.toString(), "armor");
                log.add("You obtained the armor: " + armorDrop.getClass().getSimpleName());
            }

            claim.put(PLAYER, basePlayer);
        }
        claim.put("log", log);
        return ResponseEntity.ok(jwtGen.getToken(claim));
    }

    @PostMapping(path = "/equip-weapon/{weaponName}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> equipWeapon(@PathVariable (value = "weaponName") String weaponName, @RequestBody Map<String, String> bodyReq){
        String tokenPlayer = bodyReq.get("data");
        Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
        var player = new ObjectMapper().convertValue(claim.get(PLAYER), Player.class);
        player.useEquipment(weaponName, "weapon");
        claim.put(PLAYER, player);
        return ResponseEntity.ok(jwtGen.getToken(claim));
    }

    @PostMapping(path = "/equip-armor/{armorName}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> equipArmor(@PathVariable (value = "armorName") String armorName, @RequestBody Map<String, String> bodyReq){
        String tokenPlayer = bodyReq.get("data");
        Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
        var player = new ObjectMapper().convertValue(claim.get(PLAYER), Player.class);
        player.useEquipment(armorName, "armor");
        claim.put(PLAYER, player);
        return ResponseEntity.ok(jwtGen.getToken(claim));
    }


    public List<String> actionParse(String action) {
        return Arrays.asList(action.split("-", 2));
    }

    public int gameEnd(Player player, Monster monster) {
        if (player.getHpStat() <= 0) {
            return -1;
        }
        if (monster.getHpStat() <= 0) {
            return 1;
        }
        return 0;
    }

    public Skill skillNameToSkillObj(List<Skill> skillList, String skillName) {
        for (var i = 0; i < skillList.size(); i++) {
            if (skillName.equals(skillList.get(i).getName())) {
                return skillList.get(i);
            }
        }
        return null;
    }

    public Item itemNameToItemObj(List<Item> itemList, String itemName) {
        for (var i = 0; i < itemList.size(); i++) {
            if (itemName.equals(itemList.get(i).getName())) {
                return itemList.get(i);
            }
        }
        return null;
    }

}
