package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class HiPotion extends Item {
    public HiPotion() {
        super("Hi-Potion", "Recover HP(M)");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setHpStat(Math.min(player.getHpStat() + 250, player.getMaxHpStat()));
    }
}
