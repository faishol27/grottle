package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class IronNeedle extends Skill {

    private static final int MP_COST = 12;

    public int getMpCost() {
        return 12;
    }

    public IronNeedle() {
        super("Iron Needle", "Deal Fixed Damage(S) ");
    }

    public IronNeedle(String name, String description) {
        super(name, description);
    }


    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.setHpStat(target.getHpStat() - 125 - 14 * skillLevel);
    }
}
