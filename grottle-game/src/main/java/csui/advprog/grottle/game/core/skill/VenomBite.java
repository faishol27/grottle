package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class VenomBite extends Skill {

    private static final int MP_COST = 8;

    public int getMpCost() {
        return 8;
    }

    public VenomBite() {
        super("Venom Bite", "Deal Damage(S), Lower Target DEF(S)");
    }

    public VenomBite(String name, String description) {
        super(name, description);
    }


    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }
    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
        target.setDefStat((int) (target.getDefStat() - 8 - 0.12 * skillLevel));
    }
}
