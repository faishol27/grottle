package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class DemonTail extends Item {
    public DemonTail() {
        super("Demon Tail",
                "Deal Damage(L) to yourself, Increase ATK(MAX) for a battle");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.takeDamage(1000);
        player.setAtkStat(player.getAtkStat() + 120);
    }
}