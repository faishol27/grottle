package csui.advprog.grottle.game.core;

import java.util.Map;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import csui.advprog.grottle.game.core.ai.Monster;

public class Player implements Creature {
    @JsonProperty
    private int id;

    @JsonProperty
    private Map<String, Object> account;

    @JsonProperty
    private int currentHP;

    @JsonProperty
    private int maxHP;

    @JsonProperty
    private int currentMP;

    @JsonProperty
    private int maxMP;

    @JsonProperty
    private int attack;

    @JsonProperty
    private int defense;

    @JsonProperty
    private String skills;

    @JsonProperty
    private String items;

    @JsonProperty
    private String equipments;
    private final ObjectMapper objMap = new ObjectMapper();

    public void doAttack(Monster monster) {
        monster.takeDamage(this.attack);     
    }

    private boolean modifyItems(String item, int val) throws JsonProcessingException {
        HashMap<String, Integer> mapItem = objMap.readValue(this.items, HashMap.class);
        var freq = 0;
        if (mapItem.get(item) != null) {
            freq = mapItem.get(item);
        }
        freq += val;
        if (freq < 0 || freq > 99) {
            return false;
        }

        mapItem.put(item, freq);
        this.items = objMap.writeValueAsString(mapItem);
        return true;
    }

    public boolean addItem(String item) {
        var ret = true;
        try {
            ret = modifyItems(item, +1);
        } catch(Exception e) {
            return false;
        }    
        return ret;
    }

    public boolean useItem(String item) {
        var ret = true;
        try {
            ret = modifyItems(item, -1);
        } catch(Exception e) {
            return false;
        }
        return ret;
    }

    private int modifySkill(String skill, int val) throws JsonProcessingException {
        HashMap<String, Integer> mapSkill = objMap.readValue(this.skills, HashMap.class);
        var level = 0;
        if (mapSkill.get(skill) != null) {
            level = mapSkill.get(skill);
        }
        level += val;
        if (level < 1 || level > 20) {
            return 0;
        }

        mapSkill.put(skill, level);
        this.skills = objMap.writeValueAsString(mapSkill);
        return level;
    }

    public boolean addSkill(String skill) {
        var ret = 0;
        try {
            ret = modifySkill(skill, +1);
        } catch(Exception e) {
            return false;
        }    
        return ret > 0;
    }

    public int useSkill(String skill) {
        var ret = 0;
        try {
            ret = modifySkill(skill, 0);
        } catch(Exception e) {
            return 0;
        }
        return ret;
    }

    //Lucky added this
    public int getSkillLevel(String skill)  throws JsonProcessingException{
        if (useSkill(skill) == 0) {
            return modifySkill(skill, 0);
        }
        return -1;
    }

    private boolean modifyEquipment(String name, String type, int signal) throws JsonProcessingException {
        HashMap<String, Object> mapEquip = objMap.readValue(this.equipments, HashMap.class);
        HashMap<String, Boolean> mapType = objMap.convertValue(mapEquip.get(type), HashMap.class);
        if (mapEquip.get(type) == null) {
            mapType = new HashMap<>();
        }
        
        String activeEquipment = null;
        for (Map.Entry<String,Boolean> entry : mapType.entrySet()) {
            if (Boolean.TRUE.equals(entry.getValue())) {
                activeEquipment = entry.getKey();
            }
        }

        if (signal == 0) {
            mapType.put(name, false);
            if(activeEquipment != null) {
                mapType.put(activeEquipment, true);
            }
        } else {
            if(activeEquipment != null) {
                mapType.put(activeEquipment, false);
            }
            if (mapType.get(name) == null) return false;
            mapType.put(name, true);
        }

        mapEquip.put(type, mapType);
        this.equipments = objMap.writeValueAsString(mapEquip);
        return true;
    }

    public boolean addEquipment(String name, String type) {
        try {
            modifyEquipment(name, type, 0);
        } catch(Exception e) {
            return false;
        }    
        return true;
    }

    public boolean useEquipment(String name, String type) {
        var ret = true;
        try {
            ret = modifyEquipment(name, type, 1);
        } catch(Exception e) {
            return false;   
        }
        return ret;
    }
   
    public void takeDamage(int dmg){
        dmg = Math.max(1, dmg - this.defense);
        this.currentHP -= dmg;
    }

    public void setAtkStat(int atkStat) {
        this.attack = atkStat;
    }

    public void setDefStat(int defStat) {
        this.defense = defStat;
    }

    public void setHpStat(int hpStat) {
        this.currentHP = hpStat;
    }

    public void setMpStat(int mpStat) {
        this.currentMP = mpStat;
    }

    public void setMaxHpStat(int maxHpStat) {
        this.maxHP = maxHpStat;
    }

    public void setMaxMpStat(int maxMpStat) {
        this.maxMP = maxMpStat;
    }

    @JsonIgnore
    public int getAtkStat() {
        return this.attack;
    }

    @JsonIgnore
    public int getDefStat() {
        return this.defense;
    }

    @JsonIgnore
    public int getHpStat() {
        return this.currentHP;
    }

    @JsonIgnore
    public int getMpStat() {
        return this.currentMP;
    }

    @JsonIgnore
    public int getMaxHpStat() {
        return this.maxHP;
    }
    
    @JsonIgnore
    public int getMaxMpStat() {
        return this.maxMP;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setAccount(Map<String, Object> account) {
        this.account = account;
    }
}
