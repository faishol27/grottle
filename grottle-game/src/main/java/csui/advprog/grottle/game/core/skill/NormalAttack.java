package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class NormalAttack extends Skill {

    public NormalAttack() {
        super("Normal Attack",  "Deal Standard Damage");
    }

    @Override
    public int getMpCost() {
        return 0;
    }

    public NormalAttack(String name, String description) {
        super(name, description);
    }

    public void execute(Creature attacker, Creature target, int skillLevel) {
        target.takeDamage(attacker.getAtkStat());
    }

}
