package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class Blizzard extends Skill{

    private static final int MP_COST = 7;

    public int getMpCost() {
        return 7;
    }

    public Blizzard() {
        super("Blizzard", "Lower Target's DEF(S), Deal Damage(S)");
    }

    public Blizzard(String name, String description) {
        super(name, description);
    }

    private int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel){
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.setDefStat(target.getDefStat() - 5);
    }
}