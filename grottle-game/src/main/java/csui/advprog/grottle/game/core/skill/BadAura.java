package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class BadAura extends Skill {

    public BadAura() {
        super("Bad Aura", "You feel a glooming presence over you");
    }

    @Override
    public int getMpCost() {
        return 0;
    }

    public BadAura(String name, String description) {
        super(name, description);
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        //do nothing
    }

}
