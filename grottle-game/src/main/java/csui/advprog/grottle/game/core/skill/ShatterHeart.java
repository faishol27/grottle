package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class ShatterHeart extends Skill{

    private static final int MP_COST = 80;

    public int getMpCost() {
        return 80;
    }

    public ShatterHeart() {
        super("Shatter Heart", "Lower Target's DEF(L), Deal Damage(M)");
    }

    public ShatterHeart(String name, String description) {
        super(name, description);
    }


    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.3 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        target.setDefStat(target.getDefStat() - 80);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));

    }
}
