package csui.advprog.grottle.game.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/test/jwt")
public class JwtController {
    @GetMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> jwtTest() {
        return ResponseEntity.ok("sukses");
    }
}
