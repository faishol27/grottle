package csui.advprog.grottle.game.core;

public interface Creature {
    public void takeDamage(int damage);
    public void setAtkStat(int atkStat);
    public void setDefStat(int defStat);
    public void setHpStat(int hpStat);
    public void setMpStat(int mpStat);
    public void setMaxHpStat(int maxHpStat);
    public void setMaxMpStat(int maxMpStat);
    public int getAtkStat();
    public int getDefStat();
    public int getHpStat();
    public int getMpStat();
    public int getMaxHpStat();
    public int getMaxMpStat();
}
