package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class FullminatingDarkness extends Skill {

    private static final int MP_COST = 66;

    public int getMpCost() {
        return 66;
    }

    public FullminatingDarkness() {
        super("Fullminating Darkness",
                "Increase DEF(XL), Deal damage(S)");
    }

    public FullminatingDarkness(String name, String description) {
        super(name, description);
    }


    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.1 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setDefStat(attacker.getDefStat() + 150);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
    }
}
