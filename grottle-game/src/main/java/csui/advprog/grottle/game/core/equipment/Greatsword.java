package csui.advprog.grottle.game.core.equipment;

public class Greatsword implements Weapon {
    private int attackStat = 7;
    private int defenseStat = 3;

    @Override
    public int getAttackStat() {
        return this.attackStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }

}
