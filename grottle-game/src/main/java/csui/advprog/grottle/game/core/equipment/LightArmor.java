package csui.advprog.grottle.game.core.equipment;

public class LightArmor implements Armor {
    private int hpStat = 25;
    private int defenseStat = 5;

    @Override
    public int getHpStat() {
        return this.hpStat;
    }

    @Override
    public int getDefenseStat() {
        return this.defenseStat;
    }

}
