package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.skill.*;

import java.util.ArrayList;
import java.util.List;

public class ArmoredSoul extends Monster {
    public ArmoredSoul(String name, int maxHpStat, int maxMpStat, int atkStat, int defStat) {
        super(name, maxHpStat, maxMpStat, atkStat, defStat, true);
    }

    public ArmoredSoul() {
        super();
    }

    @Override
    public void recover() {
        if (this.getHpStat() + (int) (2.3*this.getTurnCount()) > this.getMaxHpStat()) {
            this.setHpStat(this.getMaxHpStat());
        } else {
            this.setHpStat(this.getHpStat() + (int) (2.3*this.getTurnCount()));
            this.setRagePoint(this.getRagePoint() + this.getMaxHpStat() - this.getHpStat());
        }

        if (this.getMpStat() + 15 > this.getMaxMpStat()) {
            this.setMpStat(this.getMaxMpStat());
        } else {
            this.setMpStat(this.getMpStat() + 15);
        }
    }

    @Override
    public String generateRageDescription() {
        return "Armoured Soul sacrifice its HP(S) for ATK(S), DEF(S), and MP(S)!";
    }

    @Override
    int generateRagePoint() {
        return this.getMaxHpStat() + this.getMaxMpStat();
    }

    @Override
    public int attack() {
        this.setRagePoint(this.getRagePoint() + 20);
        return (int) ((this.getAtkStat() + this.getMpStat()) * 1.2);
    }

    @Override
    public void rage() {
        this.setAtkStat(this.getAtkStat() + (int) (this.getAtkStat() / 33.0));
        this.setDefStat(this.getDefStat() + (int) (this.getDefStat() / 33.0));
        this.setHpStat(this.getHpStat() - (int) (5.0/100*this.getMaxHpStat()));
        this.setMpStat(this.getMpStat() + (int) (5.0/100*this.getMaxHpStat()));
        this.setRagePoint(this.getRagePoint() - (int) (0.33 * this.getMaxHpStat()));
    }

    @Override
    List<Skill> generateSkillList(){
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new BadAura());
        skillList.add(new MaterializeArmiger());
        skillList.add(new HazardForm());
        skillList.add(new Recharge());
        skillList.add(new HardSlash());
        skillList.add(new RecklessFlurry());
        return skillList;
    }

    @Override
    List<String> generateTurnOrder() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Skill-Bad Aura");
        turnOrder.add("Skill-Materialize Armiger");
        turnOrder.add("Skill-Hazard Form");
        turnOrder.add("Skill-Hard Slash");
        turnOrder.add("Skill-Hard Slash");
        turnOrder.add("Skill-Reckless Flurry");
        turnOrder.add("Attack");
        return turnOrder;
    }
}
