package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class ManaSiphon extends Skill {

    private static final int MP_COST = 30;

    public int getMpCost() {
        return 30;
    }

    public ManaSiphon() {
        super("Mana Siphon", "Deal MP Damage(M), Recover MP sesuai damage");
    }

    public ManaSiphon(String name, String description) {
        super(name, description);
    }

    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.3 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        int mpDamage = dealDamage(attacker.getAtkStat(), skillLevel);
        target.setMpStat(Math.max(target.getMpStat() - mpDamage, 0));

        attacker.setMpStat(Math.min(attacker.getMpStat() + mpDamage, attacker.getMaxMpStat()));

    }
}