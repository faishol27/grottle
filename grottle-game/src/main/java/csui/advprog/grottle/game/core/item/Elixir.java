package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class Elixir extends Item{

    public Elixir() {
        super("Elixir", "Recover Max HP and MPRecover Max HP and MP");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setHpStat(player.getMaxHpStat());
        player.setMpStat(player.getMaxMpStat());
    }
}
