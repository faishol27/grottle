package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class RecklessFlurry extends Skill{

    private static final int MP_COST = 14;

    public int getMpCost() {
        return 14;
    }

    public RecklessFlurry() {
        super("Reckless Flury", "Raise ATK(S), Deal Damage(M)");
    }

    public RecklessFlurry(String name, String description) {
        super(name, description);
    }

    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.3 + getMultGrowth() * level));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setAtkStat((int) (attacker.getAtkStat() + 6 + 0.125 * skillLevel));
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));
    }
}
