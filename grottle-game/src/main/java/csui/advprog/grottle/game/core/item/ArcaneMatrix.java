package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class ArcaneMatrix extends Item{
    public ArcaneMatrix(){
        super("Arcane Matrix", "Deal MP Damage(L) to monster");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        monster.setMpStat(monster.getMpStat() - 60);
    }

}
