package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;

public class Ether extends Item {

    public Ether() {
        super("Ether", "Recover MP(S)");
    }

    @Override
    public void execute(Creature player, Creature monster, Creature basePlayer) {
        player.setMpStat(Math.min(player.getMpStat() + 20, player.getMaxMpStat()));
    }
}
