package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;

public class MaterializeArmiger extends Skill{

    private static final int MP_COST = 120;

    public int getMpCost() {
        return 120;
    }

    public MaterializeArmiger() {
        super("Materialize: Armiger", "Increase ATK(L), Deal Damage(XL)");
    }

    public MaterializeArmiger(String name, String description) {
        super(name, description);
    }


    public int dealDamage(int atkStat, int level) {
        return (int)Math.floor(atkStat * (1.7 + (getMultGrowth() * (level - 1))));
    }

    @Override
    public void execute(Creature attacker, Creature target, int skillLevel) {
        attacker.setMpStat(attacker.getMpStat() - MP_COST);
        attacker.setAtkStat(attacker.getAtkStat() + 100);
        target.takeDamage(dealDamage(attacker.getAtkStat(), skillLevel));

    }
}
