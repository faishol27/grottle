package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.DrainStrike;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.*;

class DrainStrikeTest {

    private DrainStrike skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new DrainStrike();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000); player.setMaxHpStat(500);player.setAtkStat(200);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        assertEquals(67, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 2);
        assertEquals(120, monster.getHpStat());
        assertEquals(240, player.getHpStat());
    }
}