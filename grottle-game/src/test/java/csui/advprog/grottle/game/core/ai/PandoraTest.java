package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PandoraTest {
    private Class<?> pandoraClass;
    private Pandora pandoraMonster;

    @BeforeEach
    void setUp() throws Exception {
        pandoraClass = Class.forName("csui.advprog.grottle.game.core.ai.Pandora");
        pandoraMonster = new Pandora("Pandora", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        pandoraMonster.setHpStat(999);
        pandoraMonster.setMpStat(999);
        pandoraMonster.setTurnCount(10);

        pandoraMonster.recover();
        assertEquals(1000, pandoraMonster.getHpStat());
        assertEquals(1000, pandoraMonster.getMpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        pandoraMonster.setHpStat(100);
        pandoraMonster.setMpStat(100);
        pandoraMonster.setTurnCount(10);

        pandoraMonster.recover();
        assertEquals(100 + (int) (1.7 * 10), pandoraMonster.getHpStat());
        assertEquals(100 + 30, pandoraMonster.getMpStat());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(pandoraMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals(pandoraMonster.getMaxHpStat(), pandoraMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        pandoraMonster.setMpStat(100);
        pandoraMonster.setRagePoint(0);
        pandoraMonster.setAtkStat(100);
        int damageDealt = (int) ((pandoraMonster.getMpStat() - 45) * 0.15 + pandoraMonster.getAtkStat() * 0.95);
        assertEquals(damageDealt, pandoraMonster.attack());
        assertEquals(55, pandoraMonster.getMpStat());
        assertEquals(10, pandoraMonster.getRagePoint());

        pandoraMonster.setMpStat(10);
        pandoraMonster.attack();
        assertEquals(0, pandoraMonster.getMpStat());
    }

    @Test
    void testPandoraIsABoss() {
        assertEquals(true, pandoraMonster.isBoss());
    }

    @Test
    void testPandoraHasSkills() {
        assertNotEquals(0, pandoraMonster.getSkillList().size());
        assertEquals(5, pandoraMonster.getSkillList().size());
    }

    @Test
    void testPandoraHasTurnOrder() {
        assertNotEquals(0, pandoraMonster.getTurnOrder().size());
        assertEquals(7, pandoraMonster.getTurnOrder().size());
    }

    @Test
    void testRageActuallyWork() {
        pandoraMonster.setHpStat(1);
        pandoraMonster.setAtkStat(10);
        pandoraMonster.setMaxHpStat(1000);
        pandoraMonster.setRagePoint(1000);
        pandoraMonster.rage();
        assertEquals(11, pandoraMonster.getHpStat());
        assertEquals(960, pandoraMonster.getRagePoint());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Pandora is absorbing dead souls! Recovering HP(S)!", pandoraMonster.getRageDescription());
    }
}
