package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class MendWoundTest {

    private MendWound skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new MendWound();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100); player.setMaxHpStat(1000);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute(){
        assertEquals(88, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(88, player.getMpStat());
        assertEquals(7, player.getDefStat());
        assertEquals(87,player.getHpStat());
    }
}