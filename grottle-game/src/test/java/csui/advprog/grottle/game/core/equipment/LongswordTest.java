package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class LongswordTest {
    private Class<?> test;
    Longsword longswordTest = new Longsword();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Longsword");
    }

    @Test
    void testLongswordIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testLongswordAttackStat() {
        assertEquals(5, longswordTest.getAttackStat());
    }

    @Test
    void testLongswordDefenseStat() {
        assertEquals(5, longswordTest.getDefenseStat());
    }

    @Test
    void testLongswordIsAnEquipment() {
        assertTrue(longswordTest instanceof Equipment);
    }

}
