package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class OmegaMachineTest {
    private Class<?> omegaMachineClass;
    private OmegaMachine omegaMachineMonster;

    @BeforeEach
    void setUp() throws Exception {
        omegaMachineClass = Class.forName("csui.advprog.grottle.game.core.ai.OmegaMachine");
        omegaMachineMonster = new OmegaMachine("omegaMachine", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        omegaMachineMonster.setHpStat(999);
        omegaMachineMonster.setMpStat(999);

        omegaMachineMonster.recover();
        assertEquals(1000, omegaMachineMonster.getHpStat());
        assertEquals(1000, omegaMachineMonster.getMpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        omegaMachineMonster.setHpStat(100);
        omegaMachineMonster.setMaxHpStat(1000);
        omegaMachineMonster.setMaxMpStat(1000);
        omegaMachineMonster.setMpStat(100);
        omegaMachineMonster.setRagePoint(0);

        omegaMachineMonster.recover();
        assertEquals(100 + (int) (omegaMachineMonster.getMaxHpStat() / 30.0), omegaMachineMonster.getHpStat());
        assertEquals(100 + (int) (omegaMachineMonster.getMaxMpStat() / 50.0), omegaMachineMonster.getMpStat());
        assertEquals(omegaMachineMonster.generateRagePoint(), omegaMachineMonster.getRagePoint());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(omegaMachineMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals(33,
                omegaMachineMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        omegaMachineMonster.setAtkStat(100);

        assertEquals(333 ,omegaMachineMonster.attack());
    }

    @Test
    void testRageActuallyWork() {
        omegaMachineMonster.setMaxMpStat(1000);
        omegaMachineMonster.setMaxHpStat(1000);
        omegaMachineMonster.setRagePoint(1001);

        omegaMachineMonster.rage();
        assertEquals(1000 + (int) (0.04 * 1000), omegaMachineMonster.getMaxHpStat());
    }

    @Test
    void testOmegaMachineIsABoss() {
        assertEquals(true, omegaMachineMonster.isBoss());
    }

    @Test
    void testOmegaMachineHasSkills() {
        assertNotEquals(0, omegaMachineMonster.getSkillList().size());
        assertEquals(7, omegaMachineMonster.getSkillList().size());
    }

    @Test
    void testOmegaMachineHasTurnOrder() {
        assertNotEquals(0, omegaMachineMonster.getTurnOrder().size());
        assertEquals(7, omegaMachineMonster.getTurnOrder().size());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Omega Machine starts upgrading itself! Increase MAX HP(MIN)!", omegaMachineMonster.getRageDescription());
    }
}
