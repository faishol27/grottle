package csui.advprog.grottle.game.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import csui.advprog.grottle.game.core.ai.Monster;

@ExtendWith(MockitoExtension.class)
class PlayerTest {
    @Mock
    private Monster monsterMock;
    private Player player = new Player();

    @Test
    void testPlayerDoAttackCorrect() throws Exception {
        player.doAttack(monsterMock);
        verify(monsterMock, times(1)).takeDamage(anyInt());
    }

    @Test
    void testPlayerAddItemCorrect() throws Exception {
        var items = "{\"item1\":0}";
        ReflectionTestUtils.setField(player, "items", items);
        boolean ret = player.addItem("item1");
        String itemsNew = (String) ReflectionTestUtils.getField(player, "items");
        assertNotEquals(itemsNew, items);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddItemNew() throws Exception {
        var items = "{}";
        ReflectionTestUtils.setField(player, "items", items);
        boolean ret = player.addItem("item1");
        String itemsNew = (String) ReflectionTestUtils.getField(player, "items");
        assertNotEquals(itemsNew, items);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddItemMaxBound() throws Exception {
        var items = "{\"item1\":99}";
        ReflectionTestUtils.setField(player, "items", "{\"item1\":99}");
        boolean ret = player.addItem("item1");
        String itemsNew = (String) ReflectionTestUtils.getField(player, "items");
        assertEquals(itemsNew, items);
        assertFalse(ret);
    }

    @Test
    void testPlayerAddItemInvalidJSON() throws Exception {
        var items = "{'item1':0}";
        ReflectionTestUtils.setField(player, "items", items);
        boolean ret = player.addItem("item1");
        String itemsNew = (String) ReflectionTestUtils.getField(player, "items");
        assertEquals(itemsNew, items);
        assertFalse(ret);
    }

    @Test
    void testPlayerUseItemCorrect() throws Exception {
        var items = "{\"item1\":99}";
        ReflectionTestUtils.setField(player, "items", items);
        boolean ret = player.useItem("item1");
        String itemsNew = (String) ReflectionTestUtils.getField(player, "items");
        assertNotEquals(itemsNew, items);
        assertTrue(ret);
    }

    @Test
    void testPlayerUseItemInvalidItem() throws Exception {
        ReflectionTestUtils.setField(player, "items", "");
        boolean ret = player.useItem("item1");
        assertFalse(ret);
    }

    @Test
    void testPlayerUseItemInvalidJSON() throws Exception {
        ReflectionTestUtils.setField(player, "items", "{'item1':0}");
        boolean ret = player.useItem("item1");
        assertFalse(ret);
    }

    @Test
    void testPlayerAddSkillCorrect() throws Exception {
        var skills = "{\"skill1\":0}";
        ReflectionTestUtils.setField(player, "skills", skills);
        boolean ret = player.addSkill("skill1");
        String skillsNew = (String) ReflectionTestUtils.getField(player, "skills");
        assertNotEquals(skillsNew, skills);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddSkillNew() throws Exception {
        var skills = "{}";
        ReflectionTestUtils.setField(player, "skills", skills);
        boolean ret = player.addSkill("skill1");
        String skillsNew = (String) ReflectionTestUtils.getField(player, "skills");
        assertNotEquals(skillsNew, skills);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddSkillMaxBound() throws Exception {
        var skills = "{\"skill1\":99}";
        ReflectionTestUtils.setField(player, "skills", skills);
        boolean ret = player.addSkill("skill1");
        String skillsNew = (String) ReflectionTestUtils.getField(player, "skills");
        assertEquals(skillsNew, skills);
        assertFalse(ret);
    }

    @Test
    void testPlayerAddSkillInvalidJSON() throws Exception {
        ReflectionTestUtils.setField(player, "skills", "{'skill1':0}");
        boolean ret = player.addSkill("skill1");
        assertFalse(ret);
    }

    @Test
    void testPlayerUseSkillCorrect() throws Exception {
        var skills = "{\"skill1\":10}";
        ReflectionTestUtils.setField(player, "skills", skills);
        int ret = player.useSkill("skill1");
        String skillsNew = (String) ReflectionTestUtils.getField(player, "skills");
        assertEquals(skillsNew, skills);
        assertTrue(ret > 0);
    }

    @Test
    void testPlayerUseSkillInvalidItem() throws Exception {
        ReflectionTestUtils.setField(player, "skills", "");
        int ret = player.useSkill("skill1");
        assertEquals(0, ret);
    }

    @Test
    void testPlayerUseSkillInvalidJSON() throws Exception {
        ReflectionTestUtils.setField(player, "skills", "{'skill1':0}");
        int ret = player.useSkill("skill1");
        assertEquals(0, ret);
    }

    @Test
    void testPlayerAddEquipmentCorrect() throws Exception {
        var equips = "{\"armor\":{}}";
        ReflectionTestUtils.setField(player, "equipments", equips);
        boolean ret = player.addEquipment("equip1", "armor");
        String equipNew = (String) ReflectionTestUtils.getField(player, "equipments");
        assertNotEquals(equips, equipNew);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddEquipmentEquipped() throws Exception {
        var equips = "{\"armor\":{\"equip1\":true}}";
        ReflectionTestUtils.setField(player, "equipments", equips);
        boolean ret = player.addEquipment("equip1", "armor");
        String equipNew = (String) ReflectionTestUtils.getField(player, "equipments");
        assertEquals(equips, equipNew);
        assertTrue(ret);
    }

    @Test
    void testPlayerAddEquipmentInvalidJSON() throws Exception {
        ReflectionTestUtils.setField(player, "equipments", "{'equip':0}");
        boolean ret = player.addEquipment("equip1", "armor");
        assertFalse(ret);
    }

    @Test
    void testPlayerUseEquipmentCorrect() throws Exception {
        var equips = "{\"armor\":{\"equip1\":false}}";
        ReflectionTestUtils.setField(player, "equipments", equips);
        boolean ret = player.useEquipment("equip1", "armor");
        String equipNew = (String) ReflectionTestUtils.getField(player, "equipments");
        assertNotEquals(equips, equipNew);
        assertTrue(ret);
    }

    @Test
    void testPlayerUseEquipmentChange() throws Exception {
        var equips = "{\"armor\":{\"equip1\":true,\"equip2\":false}}";
        ReflectionTestUtils.setField(player, "equipments", equips);
        boolean ret = player.useEquipment("equip2", "armor");
        String equipNew = (String) ReflectionTestUtils.getField(player, "equipments");
        assertNotEquals(equips, equipNew);
        assertTrue(ret);
    }

    @Test
    void testPlayerUseEquipmentNotFound() throws Exception {
        var equips = "{\"armor\":{\"equip1\":true}}";
        ReflectionTestUtils.setField(player, "equipments", equips);
        boolean ret = player.useEquipment("equip2", "armor");
        String equipNew = (String) ReflectionTestUtils.getField(player, "equipments");
        assertEquals(equips, equipNew);
        assertFalse(ret);
    }

    @Test
    void testPlayerUseEquipmentInvalidJSON() throws Exception {
        ReflectionTestUtils.setField(player, "equipments", "{'equip':0}");
        boolean ret = player.useEquipment("equip1", "armor");
        assertFalse(ret);
    }



    @Test
    void testPlayerTakeDamage() throws Exception {
        ReflectionTestUtils.setField(player, "currentHP", 10000);
        player.takeDamage(1);
        int playerHp = (int) ReflectionTestUtils.getField(player, "currentHP");
        assertNotEquals(10000, playerHp);
    }

    @Test
    void testPlayerSetAtkStat() throws Exception {
        player.setAtkStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "attack");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerSetDefStat() throws Exception {
        player.setDefStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "defense");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerSetHpStat() throws Exception {
        player.setHpStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "currentHP");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerSetMpStat() throws Exception {
        player.setMpStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "currentMP");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerSetMaxHpStat() throws Exception {
        player.setMaxHpStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "maxHP");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerSetMaxMpStat() throws Exception {
        player.setMaxMpStat(10);
        int ret = (int) ReflectionTestUtils.getField(player, "maxMP");
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetAtkStat() throws Exception {
        ReflectionTestUtils.setField(player, "attack", 10);
        int ret = player.getAtkStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetDefStat() throws Exception {
        ReflectionTestUtils.setField(player, "defense", 10);
        int ret = player.getDefStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetHpStat() throws Exception {
        ReflectionTestUtils.setField(player, "currentHP", 10);
        int ret = player.getHpStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetMaxHpStat() throws Exception {
        ReflectionTestUtils.setField(player, "maxHP", 10);
        int ret = player.getMaxHpStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetMpStat() throws Exception {
        ReflectionTestUtils.setField(player, "currentMP", 10);
        int ret = player.getMpStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerGetMaxMpStat() throws Exception {
        ReflectionTestUtils.setField(player, "maxMP", 10);
        int ret = player.getMaxMpStat();
        assertEquals(10, ret);
    }

    @Test
    void testPlayerCanDeserialize() throws Exception {
        var valInt = 10;
        var valStr = "{}";
        Map<String, Object> playerJson = new HashMap<>();
        playerJson.put("id", valInt); playerJson.put("account", new HashMap<>());
        playerJson.put("currentHP", valInt); playerJson.put("currentMP", valInt);
        playerJson.put("maxHP", valInt); playerJson.put("maxMP", valInt);
        playerJson.put("attack", valInt); playerJson.put("defense", valInt);
        playerJson.put("skills", valStr); playerJson.put("items", valStr);
        playerJson.put("equipments", valStr);

        Player player = new ObjectMapper().convertValue(playerJson, Player.class);
        assertNotNull(player);
    }

    @Test
    void testPlayerCanSerialize() throws Exception {
        var valInt = 10;
        var valStr = "{}";
        Map<String, Object> playerJson = new HashMap<>();
        playerJson.put("id", valInt); playerJson.put("account", new HashMap<>());
        playerJson.put("currentHP", valInt); playerJson.put("currentMP", valInt);
        playerJson.put("maxHP", valInt); playerJson.put("maxMP", valInt);
        playerJson.put("attack", valInt); playerJson.put("defense", valInt);
        playerJson.put("skills", valStr); playerJson.put("items", valStr);
        playerJson.put("equipments", valStr);

        Player player = new ObjectMapper().convertValue(playerJson, Player.class);
        String jsonStr = new ObjectMapper().writeValueAsString(player);
        
        String[] validKey = {"id", "currentHP", "maxHP", "attack", "skills", "equipments", "account", "currentMP", "maxMP", "items", "defense"};
        HashMap<String, Object> map = new ObjectMapper().readValue(jsonStr, HashMap.class);
        for (String elm:validKey) {
            map.remove(elm);
        }
        assertEquals(0, map.size());
    }
}
