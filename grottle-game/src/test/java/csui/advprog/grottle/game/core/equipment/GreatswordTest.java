package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class GreatswordTest {
    private Class<?> test;
    Greatsword greatswordTest = new Greatsword();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Greatsword");
    }

    @Test
    void testGreatswordIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testGreatswordAttackStat() {
        assertEquals(7, greatswordTest.getAttackStat());
    }

    @Test
    void testGreatswordDefenseStat() {
        assertEquals(3, greatswordTest.getDefenseStat());
    }

    @Test
    void testGreatswordIsAnEquipment() {
        assertTrue(greatswordTest instanceof Equipment);
    }

}
