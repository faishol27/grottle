package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class MaterializeWeaponTest {

    private MaterializeWeapon skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new MaterializeWeapon();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute(){
        assertEquals(80, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(113, player.getAtkStat());
        assertEquals(80, player.getMpStat());
    }
}