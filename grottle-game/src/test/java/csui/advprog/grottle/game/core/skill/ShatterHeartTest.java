package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ShatterHeartTest {

    private ShatterHeart skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new ShatterHeart();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute(){
        assertEquals(20, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(140, monster.getHpStat());
        assertEquals(120, monster.getDefStat());
        assertEquals(20, player.getMpStat());
    }
}