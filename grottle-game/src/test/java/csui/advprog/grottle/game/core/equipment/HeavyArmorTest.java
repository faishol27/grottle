package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class HeavyArmorTest {
    private Class<?> test;
    HeavyArmor heavyArmorTest = new HeavyArmor();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.HeavyArmor");
    }

    @Test
    void testHeavyArmorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testHeavyArmorHpStat() {
        assertEquals(50, heavyArmorTest.getHpStat());
    }

    @Test
    void testHeavyArmorDefenseStat() {
        assertEquals(10, heavyArmorTest.getDefenseStat());
    }

    @Test
    void testHeavyArmorIsAnEquipment() {
        assertTrue(heavyArmorTest instanceof Equipment);
    }
}
