package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FullminatingDarknessTest {

    private FullminatingDarkness skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new FullminatingDarkness();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,50);
    }

    @Test
    void execute(){
        assertEquals(34, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(40, monster.getHpStat());
        assertEquals(34, player.getMpStat());
        assertEquals(150, player.getDefStat());
    }
}