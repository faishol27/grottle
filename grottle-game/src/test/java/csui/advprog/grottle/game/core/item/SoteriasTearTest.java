package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.item.SoteriasTear;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class SoteriasTearTest {

    private SoteriasTear item;
    private Creature player;
    private Creature monster;
    private Creature basePlayer;

    @BeforeEach
    void setUp() {
        item = new SoteriasTear();
        player = new Player();
        basePlayer = new Player();
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        item.execute(player, monster, basePlayer);
        assertEquals(55, player.getDefStat());
        assertEquals(5, basePlayer.getDefStat());
    }
}