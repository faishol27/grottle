package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.Enroot;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class EnrootTest {

    private Enroot skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new Enroot();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);
        player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,25);
    }

    @Test
    void execute() {
        assertEquals(86, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 2);
        assertEquals(86, player.getMpStat());
        assertEquals(10, monster.getHpStat());
        assertEquals(196,monster.getAtkStat());
    }
}