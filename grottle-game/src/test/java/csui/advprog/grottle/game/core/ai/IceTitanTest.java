package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class IceTitanTest {
    private Class<?> iceTitanClass;
    private IceTitan iceTitanMonster;

    @BeforeEach
    void setUp() throws Exception {
        iceTitanClass = Class.forName("csui.advprog.grottle.game.core.ai.IceTitan");
        iceTitanMonster = new IceTitan("iceTitan", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        iceTitanMonster.setHpStat(999);
        iceTitanMonster.setDefStat(1000);

        iceTitanMonster.recover();
        assertEquals(1000, iceTitanMonster.getHpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        iceTitanMonster.setHpStat(100);
        iceTitanMonster.setDefStat(100);

        iceTitanMonster.recover();
        assertEquals(200, iceTitanMonster.getHpStat());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(iceTitanMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals(iceTitanMonster.getMaxHpStat(), iceTitanMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        iceTitanMonster.setAtkStat(100);
        iceTitanMonster.setDefStat(100);

        assertEquals((int) (0.2 * 100 + 2.5 * 100), iceTitanMonster.attack());
    }

    @Test
    void testIceTitanIsABoss() {
        assertEquals(true, iceTitanMonster.isBoss());
    }

    @Test
    void testIceTitanHasSkills() {
        assertNotEquals(0, iceTitanMonster.getSkillList().size());
        assertEquals(5, iceTitanMonster.getSkillList().size());
    }

    @Test
    void testIceTitanHasTurnOrder() {
        assertNotEquals(0, iceTitanMonster.getTurnOrder().size());
        assertEquals(10, iceTitanMonster.getTurnOrder().size());
    }

    @Test
    void testRageActuallyWork() {
        iceTitanMonster.setHpStat(50);
        iceTitanMonster.setDefStat(0);
        iceTitanMonster.setRagePoint(100);
        iceTitanMonster.rage();
        assertEquals(7, iceTitanMonster.getDefStat());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Icy shroud envelops the Titan! Increased DEF(S)!", iceTitanMonster.getRageDescription());
    }
}
