package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.ManaSiphon;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ManaSiphonTest {

    private ManaSiphon skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new ManaSiphon();
        player = new Player();
        player.setMaxMpStat(1000);player.setMpStat(100); player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,50);
    }

    @Test
    void execute(){
        assertEquals(70, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(50, monster.getMpStat());
        assertEquals(220, player.getMpStat());
    }
}