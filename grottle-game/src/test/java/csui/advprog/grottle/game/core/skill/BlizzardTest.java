package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.Blizzard;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class BlizzardTest {

    private Blizzard skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new Blizzard();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000); player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,50);
    }

    @Test
    void execute() {
        assertEquals(93, player.getMpStat() - skill.getMpCost());
        player.setAtkStat(100);
        skill.execute(player, monster, 4);
        assertEquals(93, player.getMpStat());
        assertEquals(40, monster.getHpStat());
        assertEquals(45, monster.getDefStat());
    }
}