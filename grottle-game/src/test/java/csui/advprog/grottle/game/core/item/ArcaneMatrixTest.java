package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.item.ArcaneMatrix;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ArcaneMatrixTest {

    private ArcaneMatrix item;
    private Creature player;
    private Creature monster;
    private Creature basePlayer;

    @BeforeEach
    void setUp() {
        item = new ArcaneMatrix();
        player = new Player();
        basePlayer = new Player();
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        item.execute(player, monster, basePlayer);
        assertEquals(140 , monster.getMpStat());
    }
}