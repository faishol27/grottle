package csui.advprog.grottle.game.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.HashMap;
import java.util.Map;

import csui.advprog.grottle.game.core.JWTGenerator;

@WebMvcTest(controllers = JwtController.class)
class JwtControllerTest {
    @Autowired
    private MockMvc mvc;

    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    private String token;

    @BeforeEach
    void setUp() {
        Map<String, Object> claim = new HashMap<>();
        claim.put("username", "hahaha");
        claim.put("aud", "http://localhost:8080/");
        token = jwtGen.getToken(claim);
    }

    @Test
    void testAccessNoToken() throws Exception {
        mvc.perform(get("/test/jwt"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testAccessInvalidToken() throws Exception {
        mvc.perform(get("/test/jwt").header("Authorization", "jwt.palsu"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testAccessMissmatchOrigin() throws Exception {
        mvc.perform(get("/some/url")
                    .header("Authorization", token).header("Origin", "tidak sama"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testAccessCorrect() throws Exception {
        mvc.perform(get("/test/jwt")
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isNotFound());
    }
}
