package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmoredSoulTest {
    private Class<?> armoredSoulClass;
    private ArmoredSoul armoredSoulMonster;

    @BeforeEach
    void setUp() throws Exception {
        armoredSoulClass = Class.forName("csui.advprog.grottle.game.core.ai.ArmoredSoul");
        armoredSoulMonster = new ArmoredSoul("armoredSoul", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        armoredSoulMonster.setHpStat(999);
        armoredSoulMonster.setMpStat(999);
        armoredSoulMonster.setTurnCount(10);

        armoredSoulMonster.recover();
        assertEquals(1000, armoredSoulMonster.getHpStat());
        assertEquals(1000, armoredSoulMonster.getMpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        armoredSoulMonster.setHpStat(100);
        armoredSoulMonster.setMpStat(100);
        armoredSoulMonster.setRagePoint(0);
        armoredSoulMonster.setTurnCount(10);

        armoredSoulMonster.recover();
        assertEquals(100 + (int) (2.3 * 10), armoredSoulMonster.getHpStat());
        assertEquals(100 + 15, armoredSoulMonster.getMpStat());
        assertEquals(armoredSoulMonster.getMaxHpStat() - armoredSoulMonster.getHpStat(),
                armoredSoulMonster.getRagePoint());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(armoredSoulMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals(armoredSoulMonster.getMaxHpStat() + armoredSoulMonster.getMaxMpStat(),
                armoredSoulMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        armoredSoulMonster.setAtkStat(100);
        armoredSoulMonster.setMpStat(100);
        armoredSoulMonster.setRagePoint(0);

        assertEquals((int) (200 * 1.2) ,armoredSoulMonster.attack());
        assertEquals(20, armoredSoulMonster.getRagePoint());
    }

    @Test
    void testRageActuallyWork() {
        armoredSoulMonster.setAtkStat(100);
        armoredSoulMonster.setDefStat(100);
        armoredSoulMonster.setMpStat(100);
        armoredSoulMonster.setMaxMpStat(1000);
        armoredSoulMonster.setHpStat(100);
        armoredSoulMonster.setMaxHpStat(1000);
        armoredSoulMonster.setRagePoint(1000);

        armoredSoulMonster.rage();
        assertEquals(100 + (int) (100/33.0), armoredSoulMonster.getAtkStat());
        assertEquals(100 + (int) (100/33.0), armoredSoulMonster.getDefStat());
        assertEquals(100 - (5.0 / 100 * 1000), armoredSoulMonster.getHpStat());
        assertEquals(100 + (5.0 / 100 * 1000), armoredSoulMonster.getMpStat());
    }

    @Test
    void testArmoredSoulIsABoss() {
        assertEquals(true, armoredSoulMonster.isBoss());
    }

    @Test
    void testArmoredSoulHasSkills() {
        assertNotEquals(0, armoredSoulMonster.getSkillList().size());
        assertEquals(6, armoredSoulMonster.getSkillList().size());
    }

    @Test
    void testArmoredSoulHasTurnOrder() {
        assertNotEquals(0, armoredSoulMonster.getTurnOrder().size());
        assertEquals(7, armoredSoulMonster.getTurnOrder().size());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Armoured Soul sacrifice its HP(S) for ATK(S), DEF(S), and MP(S)!", armoredSoulMonster.getRageDescription());
    }
}
