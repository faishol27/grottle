package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.BadAura;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class BadAuraTest {

    private BadAura skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new BadAura();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        skill.execute(player, monster, 4);
        assertEquals(200, monster.getDefStat());
        assertEquals(200, monster.getHpStat());
        assertEquals(200, monster.getMpStat());
        assertEquals(200, monster.getAtkStat());
    }
}