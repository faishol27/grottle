package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WildsnakeTest {
    private Class<?> wildsnakeClass;
    private Wildsnake wildsnakeMonster;

    @BeforeEach
    void setUp() throws Exception {
        wildsnakeClass = Class.forName("csui.advprog.grottle.game.core.ai.Wildsnake");
        wildsnakeMonster = new Wildsnake("Wildsnake", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        wildsnakeMonster.setHpStat(999);
        wildsnakeMonster.setMpStat(1000);

        wildsnakeMonster.recover();
        assertEquals(1000, wildsnakeMonster.getHpStat());
        assertEquals(1000, wildsnakeMonster.getMpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        wildsnakeMonster.setHpStat(100);
        wildsnakeMonster.setMpStat(100);

        wildsnakeMonster.recover();
        assertEquals(100 + 4, wildsnakeMonster.getHpStat());
        assertEquals(100 + 1, wildsnakeMonster.getMpStat());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(wildsnakeMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals((int) (wildsnakeMonster.getMaxHpStat()/8.0), wildsnakeMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        wildsnakeMonster.setAtkStat(100);
        assertEquals(100*3, wildsnakeMonster.attack());
    }

    @Test
    void testRageActuallyWork() {
        wildsnakeMonster.setAtkStat(100);
        wildsnakeMonster.setRagePoint(100);

        wildsnakeMonster.rage();
        assertEquals(0, wildsnakeMonster.getRagePoint());
        assertEquals(100 + 6, wildsnakeMonster.getAtkStat());
    }

    @Test
    void testWildsnakeIsNotABoss() {
        assertEquals(false, wildsnakeMonster.isBoss());
    }

    @Test
    void testWildsnakeHasSkills() {
        assertNotEquals(0, wildsnakeMonster.getSkillList().size());
        assertEquals(3, wildsnakeMonster.getSkillList().size());
    }

    @Test
    void testWildsnakeHasTurnOrder() {
        assertNotEquals(0, wildsnakeMonster.getTurnOrder().size());
        assertEquals(4, wildsnakeMonster.getTurnOrder().size());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Wildsnake is distressed! It gain ATK(S)!", wildsnakeMonster.getRageDescription());
    }
}
