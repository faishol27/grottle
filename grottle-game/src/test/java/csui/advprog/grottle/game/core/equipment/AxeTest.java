package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class AxeTest {
    private Class<?> test;
    Axe axeTest = new Axe();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Axe");
    }

    @Test
    void testAxeIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testAxeAttackStat() {
        assertEquals(6, axeTest.getAttackStat());
    }

    @Test
    void testAxeDefenseStat() {
        assertEquals(3, axeTest.getDefenseStat());
    }

    @Test
    void testAxeIsAnEquipment() {
        assertTrue(axeTest instanceof Equipment);
    }

}
