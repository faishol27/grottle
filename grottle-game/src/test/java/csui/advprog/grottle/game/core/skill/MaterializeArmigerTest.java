package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.MaterializeArmiger;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class MaterializeArmigerTest {

    private MaterializeArmiger skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new MaterializeArmiger();
        player = new Player();
        player.setMpStat(200); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",800,200,200,50);
    }

    @Test
    void execute(){
        assertEquals(80, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(200, player.getAtkStat());
        assertEquals(160, monster.getHpStat());
        assertEquals(80, player.getMpStat());
    }
}