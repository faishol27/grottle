package csui.advprog.grottle.game.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advprog.grottle.game.core.JWTGenerator;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.ArmoredSoul;
import csui.advprog.grottle.game.core.ai.Monster;
import csui.advprog.grottle.game.core.item.AcidicLiquid;
import csui.advprog.grottle.game.core.item.ArcaneMatrix;
import csui.advprog.grottle.game.core.item.Item;
import csui.advprog.grottle.game.core.skill.AdrenalineRush;
import csui.advprog.grottle.game.core.skill.AncientDischarge;
import csui.advprog.grottle.game.core.skill.Skill;
import csui.advprog.grottle.game.service.RoomService;

import java.nio.charset.Charset;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = RoomController.class)
class RoomControllerTest {

    @Autowired
    private RoomController roomController;

    @Autowired
    private MockMvc mvc;

    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    private String token;

    @MockBean
    private RoomService roomService;

    static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    Map<String, Object> claim = new HashMap<>();

    @BeforeEach
    void setUp() {
        claim.put("username", "hahaha");
        claim.put("aud", "http://localhost:8080/");
        token = jwtGen.getToken(claim);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testActionParse() {
        List<String> actualActionParseResult = this.roomController.actionParse("Action");
        assertEquals(1, actualActionParseResult.size());
        assertEquals("Action", actualActionParseResult.get(0));
    }

    @Test
    void testGameEnd() {
        Player player = new Player();
        assertEquals(-1, this.roomController.gameEnd(player, new ArmoredSoul("Name", 3, 3, 1, 1)));
    }

    @Test
    void testGameEnd2() {
        Player player = new Player();
        player.setHpStat(1);
        Monster monster = new ArmoredSoul("ArmoredSoul", 1, 1, 1, 1);
        monster.setHpStat(0);
        assertEquals(1, roomController.gameEnd(player, monster));
    }

    @Test
    void testGameEnd3() {
        Player player = new Player();
        player.setHpStat(1);
        Monster monster = new ArmoredSoul("ArmoredSoul", 1, 1, 1, 1);
        assertEquals(0, this.roomController.gameEnd(player, monster));
    }

    @Test
    void testSkillNameToSkillObj() {
        assertNull(this.roomController.skillNameToSkillObj(new ArrayList<Skill>(), "Skill Name"));
        assertNull(this.roomController.skillNameToSkillObj(new ArrayList<Skill>(), "Skill Name"));
        assertNull(this.roomController.skillNameToSkillObj(new ArrayList<Skill>(), "Skill Name"));
        assertNull(this.roomController.skillNameToSkillObj(new ArrayList<Skill>(), "Skill Name"));
    }

    @Test
    void testSkillNameToSkillObj2() {
        ArrayList<Skill> skillList = new ArrayList<Skill>();
        skillList.add(new AdrenalineRush());
        assertNull(this.roomController.skillNameToSkillObj(skillList, "Skill Name"));
    }

    @Test
    void testSkillNameToSkillObj3() {
        ArrayList<Skill> skillList = new ArrayList<Skill>();
        skillList.add(new AncientDischarge());
        assertNull(this.roomController.skillNameToSkillObj(skillList, "Skill Name"));
    }

    @Test
    void testSkillNameToSkillObj4() {
        ArrayList<Skill> skillList = new ArrayList<Skill>();
        skillList.add(mock(AdrenalineRush.class));
        assertNull(this.roomController.skillNameToSkillObj(skillList, "Skill Name"));
    }

    @Test
    void testSkillNameToSkillObj5() {
        ArrayList<Skill> skillList = new ArrayList<Skill>();
        AdrenalineRush adrenalineRush = new AdrenalineRush();
        skillList.add(adrenalineRush);
        assertSame(adrenalineRush, this.roomController.skillNameToSkillObj(skillList, "Adrenaline Rush"));
    }

    @Test
    void testItemNameToItemObj() {
        assertNull(this.roomController.itemNameToItemObj(new ArrayList<Item>(), "Item Name"));
        assertNull(this.roomController.itemNameToItemObj(new ArrayList<Item>(), "Item Name"));
        assertNull(this.roomController.itemNameToItemObj(new ArrayList<Item>(), "Item Name"));
        assertNull(this.roomController.itemNameToItemObj(new ArrayList<Item>(), "Item Name"));
    }

    @Test
    void testItemNameToItemObj2() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        itemList.add(new AcidicLiquid());
        assertNull(this.roomController.itemNameToItemObj(itemList, "Item Name"));
    }

    @Test
    void testItemNameToItemObj3() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        itemList.add(new ArcaneMatrix());
        assertNull(this.roomController.itemNameToItemObj(itemList, "Item Name"));
    }

    @Test
    void testItemNameToItemObj4() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        itemList.add(mock(AcidicLiquid.class));
        assertNull(this.roomController.itemNameToItemObj(itemList, "Item Name"));
    }

    @Test
    void testItemNameToItemObj5() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        AcidicLiquid acidicLiquid = new AcidicLiquid();
        itemList.add(acidicLiquid);
        assertSame(acidicLiquid, this.roomController.itemNameToItemObj(itemList, "Acidic Liquid"));
    }



}

