package csui.advprog.grottle.game.service;

import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.*;
import csui.advprog.grottle.game.core.ai.ArmoredSoul;
import csui.advprog.grottle.game.core.ai.Monster;
import csui.advprog.grottle.game.core.equipment.*;
import csui.advprog.grottle.game.core.item.*;
import csui.advprog.grottle.game.core.item.Item;
import csui.advprog.grottle.game.core.skill.*;
import csui.advprog.grottle.game.core.skill.Skill;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {RoomService.class})
@ExtendWith(MockitoExtension.class)
class RoomServiceImplTest {

    @Autowired
    private RoomService roomService;

    private Player demoPlayer;
    private Player basePlayer;
    private Pandora pandoraMonster;
    private List<Weapon> weaponLootTable;
    private List<Armor> armorLootTable;

    @BeforeEach
    void setUp() throws Exception {
        roomService = new RoomServiceImpl();

        demoPlayer = new Player();
        demoPlayer.setMaxHpStat(100);
        demoPlayer.setMaxMpStat(100);
        demoPlayer.setAtkStat(10);
        demoPlayer.setDefStat(10);
        demoPlayer.setHpStat(100);
        demoPlayer.setMpStat(100);

        basePlayer = new Player();
        basePlayer.setMaxHpStat(100);
        basePlayer.setMaxMpStat(100);
        basePlayer.setAtkStat(10);
        basePlayer.setDefStat(10);
        basePlayer.setHpStat(100);
        basePlayer.setMpStat(100);

        List<Weapon> weaponLootTable = new ArrayList<>();
        weaponLootTable.add(new Axe());
        weaponLootTable.add(new Bow());
        weaponLootTable.add(new Dagger());
        weaponLootTable.add(new Greatsword());
        weaponLootTable.add(new Longsword());

        List<Armor> armorLootTable = new ArrayList<>();
        armorLootTable.add(new HeavyArmor());
        armorLootTable.add(new LightArmor());
        armorLootTable.add(new Robe());
        pandoraMonster = new Pandora("Pandora", 100, 20, 5, 50);
    }

    @Test
    void testGetMonsterEncounter() {
        assertTrue(this.roomService.getMonsterEncounter() instanceof csui.advprog.grottle.game.core.ai.Monster);
    }

    @Test
    void testGetMonsterEncounterReturnsCorrectList() {
        List<Monster> monsters = new ArrayList<>();
        monsters.add(new Landfish("Landfish", 100, 20, 3, 3));
        monsters.add(new Wildsnake("Wildsnake", 120, 16, 4, 2));
        assertNotNull(roomService.getMonsterEncounter());
    }

    @Test
    void testGetBossEncounter() {
        RoomService roomService = new RoomServiceImpl();
        Monster actualBossEncounter = roomService.getBossEncounter();
        if (actualBossEncounter instanceof csui.advprog.grottle.game.core.ai.IceTitan) {
            int actualAtkStat = actualBossEncounter.getAtkStat();
            int actualDefStat = actualBossEncounter.getDefStat();
            int actualHpStat = actualBossEncounter.getHpStat();
            int actualMaxHpStat = actualBossEncounter.getMaxHpStat();
            int actualMaxMpStat = actualBossEncounter.getMaxMpStat();
            int actualMpStat = actualBossEncounter.getMpStat();
            String actualName = actualBossEncounter.getName();
            int actualRagePoint = actualBossEncounter.getRagePoint();
            List<Skill> skillList = actualBossEncounter.getSkillList();
            int actualTurnCount = actualBossEncounter.getTurnCount();
            assertEquals(30, actualAtkStat);
            assertTrue(actualBossEncounter.isBoss());
            List<String> turnOrder = actualBossEncounter.getTurnOrder();
            assertEquals(10, turnOrder.size());
            assertEquals("Skill-Bad Aura", turnOrder.get(0));
            assertEquals("Skill-Howl", turnOrder.get(1));
            assertEquals("Skill-Frenzy", turnOrder.get(2));
            assertEquals(0, actualTurnCount);
            assertEquals(5, skillList.size());
            Skill getResult = skillList.get(0);
            Skill getResult1 = skillList.get(1);
            Skill getResult2 = skillList.get(2);
            Skill getResult3 = skillList.get(3);
            Skill getResult4 = skillList.get(4);
            assertEquals(30, actualDefStat);
            assertEquals(2400, actualMaxHpStat);
            assertEquals(120, actualMaxMpStat);
            assertEquals("IceTitan", actualName);
            String actualName1 = getResult3.getName();
        }
        Monster monsterEncounter = roomService.getMonsterEncounter();
        if (monsterEncounter instanceof csui.advprog.grottle.game.core.ai.Landfish) {
            assertTrue(monsterEncounter instanceof csui.advprog.grottle.game.core.ai.Landfish);
            assertEquals(100, monsterEncounter.getHpStat());
            assertEquals(3, monsterEncounter.getDefStat());
            assertEquals(3, monsterEncounter.getAtkStat());
        }
    }

    @Test
    void testGetBossEncounter2() {
        assertTrue(this.roomService.getBossEncounter() instanceof csui.advprog.grottle.game.core.ai.Monster);
    }

    @Test
    void testGetBossEncounterReturnsCorrectList() {
        List<Monster> boss = new ArrayList<>();
        boss.add(new ArmoredSoul("ArmoredSoul", 2000, 300, 50, 24));
        boss.add(new DruidOfTheTree("DruidOfTheTree", 1800, 600, 40, -20));
        boss.add(new IceTitan("IceTitan", 2400, 120, 30, 30));
        boss.add(new OmegaMachine("OmegaMachine", 2000, 400, 75, 12));
        boss.add(new Pandora("Pandora", 2666, 666, 66, 66));

        assertNotNull(roomService.getBossEncounter());
    }

    @Test
    void testGetItemLoot() {
        Item getResult = this.roomService.getItemLoot(new ArmoredSoul("Acidic Liquid", 3, 3, 1, 1)).get(0);
        assertEquals("Raise MAX HP(M), MAX MP(M), ATK(M), DEF(M) permanently", getResult.getDescription());
        assertEquals("Soul Fragment", getResult.getName());
    }

    @Test
    void testGetItemLoot2() {
        Item getResult = this.roomService.getItemLoot(new ArmoredSoul("Acidic Liquid", 3, 3, 1, 1)).get(0);
        assertEquals("Soul Fragment", getResult.getName());
        assertEquals("Raise MAX HP(M), MAX MP(M), ATK(M), DEF(M) permanently", getResult.getDescription());
    }

    @Test
    void testGetItemLoot3() {
        Item getResult = this.roomService.getItemLoot(new ArmoredSoul("Acidic Liquid", 3, 3, 1, 1)).get(0);
        assertEquals("Soul Fragment", getResult.getName());
        assertEquals("Raise MAX HP(M), MAX MP(M), ATK(M), DEF(M) permanently", getResult.getDescription());
    }

    @Test
    void testGetMonsterSkills() {
        assertEquals(6, this.roomService.getMonsterSkills(new ArmoredSoul("Name", 3, 3, 1, 1)).size());
        assertEquals(6, this.roomService.getMonsterSkills(new ArmoredSoul("Name", 3, 3, 1, 1)).size());
        assertEquals(6, this.roomService.getMonsterSkills(new ArmoredSoul("Name", 3, 3, 1, 1)).size());
        assertEquals(6, this.roomService.getMonsterSkills(new ArmoredSoul("Name", 3, 3, 1, 1)).size());
    }

    @Test
    void testGetMonsterSkills2() {
        Monster monster = mock(Monster.class);
        ArrayList<Skill> skillList = new ArrayList<Skill>();
        when(monster.getSkillList()).thenReturn(skillList);
        List<Skill> actualMonsterSkills = this.roomService.getMonsterSkills(monster);
        assertSame(skillList, actualMonsterSkills);
        assertTrue(actualMonsterSkills.isEmpty());
        verify(monster).getSkillList();
    }

    @Test
    void testGetWeaponDrop() {
        Weapon weapon = roomService.getWeaponDrop();
        while (weapon != null) {
            weapon = roomService.getWeaponDrop();
        }
        assertNull(this.roomService.getWeaponDrop());
    }

    @Test
    void testWeaponDropsActualWeaponWhenRngIsFavoured() {
        Weapon weapon = roomService.getWeaponDrop();
        while (weapon == null) {
            weapon = roomService.getWeaponDrop();
        }
        assertTrue(weapon instanceof csui.advprog.grottle.game.core.equipment.Weapon);

        weapon = roomService.getWeaponDrop();
        while (weapon == null) {
            weapon = roomService.getWeaponDrop();
        }
        assertTrue(weapon instanceof csui.advprog.grottle.game.core.equipment.Weapon);
    }

    @Test
    void testGetArmorDrop() {
        Armor armor = roomService.getArmorDrop();
        while (armor != null) {
            armor = roomService.getArmorDrop();
        }
        assertNull(this.roomService.getArmorDrop());
    }

    @Test
    void testArmorDropsActualArmorWhenRngIsFavoured() {
        Armor armor = roomService.getArmorDrop();
        while (armor == null) {
            armor = roomService.getArmorDrop();
        }
        assertTrue(armor instanceof csui.advprog.grottle.game.core.equipment.Armor);

        armor = roomService.getArmorDrop();
        while (armor == null) {
            armor = roomService.getArmorDrop();
        }
        assertTrue(armor instanceof csui.advprog.grottle.game.core.equipment.Armor);
    }

    @Test
    void testPlayerRecover() {
        Player player = new Player();
        player.setMaxHpStat(100);
        player.setHpStat(10);
        player.setMaxMpStat(100);
        player.setMpStat(10);
        player = roomService.playerRecover(player);
        assertEquals(10 + 5 + player.getMaxHpStat() / 10, player.getHpStat());
        assertEquals(10 + 2 + player.getMaxMpStat() / 20, player.getMpStat());
    }

    @Test
    void testPlayerRecoverDoesNotoverflow() {
        Player player = new Player();
        player.setMaxHpStat(100);
        player.setHpStat(99);
        player.setMaxMpStat(100);
        player.setMpStat(99);
        player = roomService.playerRecover(player);
        assertEquals(105, player.getHpStat());
        assertEquals(102, player.getMpStat());

    }

    @Test
    void testGenerateLoots() {
        RoomServiceImpl roomServiceImpl = new RoomServiceImpl();
        roomServiceImpl.generateLoots();
        assertEquals(14, this.roomService.getItemList().size());
    }

    @Test
    void testGenerateSkills() {
        RoomServiceImpl roomServiceImpl = new RoomServiceImpl();
        roomServiceImpl.generateSkills();
        assertEquals(27, this.roomService.getSkillList().size());
    }


    @Test
    void testGetSkillList() {
        List<Skill> actualSkillList = this.roomService.getSkillList();
        assertEquals(27, actualSkillList.size());
        assertEquals("Deal Damage(S), Lower Target ATK(MIN)", actualSkillList.get(5).getDescription());
        Skill getResult = actualSkillList.get(2);
        assertEquals("Bad Aura", getResult.getName());
        assertEquals("You feel a glooming presence over you", getResult.getDescription());
        Skill getResult1 = actualSkillList.get(25);
        assertEquals("Venom Bite", getResult1.getName());
        assertEquals("Deal Damage(S), Lower Target DEF(S)", getResult1.getDescription());
        Skill getResult2 = actualSkillList.get(24);
        assertEquals("Shatter Heart", getResult2.getName());
        assertEquals("Lower Target's DEF(L), Deal Damage(M)", getResult2.getDescription());
        Skill getResult3 = actualSkillList.get(4);
        assertEquals("Drain Strike", getResult3.getName());
        assertEquals("Deal Damage(L), recover HP according to damage", getResult3.getDescription());
        Skill getResult4 = actualSkillList.get(23);
        assertEquals("Reckless Flury", getResult4.getName());
        assertEquals("Raise ATK(S), Deal Damage(M)", getResult4.getDescription());
        Skill getResult5 = actualSkillList.get(3);
        assertEquals("Blizzard", getResult5.getName());
        Skill getResult6 = actualSkillList.get(0);
        assertEquals("Recover HP(M), Raise ATK(M)", getResult6.getDescription());
        assertEquals("Lower Target's DEF(S), Deal Damage(S)", getResult5.getDescription());
        Skill getResult7 = actualSkillList.get(1);
        assertEquals("Ancient Discharge", getResult7.getName());
        assertEquals("Deal Damage(M) to self, Recover MP according to damage taken", getResult7.getDescription());
        Skill getResult8 = actualSkillList.get(26);
        assertEquals("Volcano Eruption", getResult8.getName());
        assertEquals("Deal Damage(L), Lower Target's DEF(L)", getResult8.getDescription());
        assertEquals("Adrenaline Rush", getResult6.getName());
    }

    @Test
    void testGetItemList() {
        List<Item> actualItemList = this.roomService.getItemList();
        assertEquals(14, actualItemList.size());
        Item getResult = actualItemList.get(9);
        assertEquals("Potion", getResult.getName());
        Item getResult1 = actualItemList.get(8);
        assertEquals("Hi-Potion", getResult1.getName());
        assertEquals("Recover HP(M)", getResult1.getDescription());
        Item getResult2 = actualItemList.get(5);
        assertEquals("Recover Max HP and MPRecover Max HP and MP", getResult2.getDescription());
        Item getResult3 = actualItemList.get(2);
        assertEquals("Raise ATK(M) for 1 battle, Raise ATK(S) permanently", getResult3.getDescription());
        Item getResult4 = actualItemList.get(12);
        assertEquals("Recover MP(M), Raise MAX MP(S) permanently", getResult4.getDescription());
        Item getResult5 = actualItemList.get(11);
        assertEquals("Deal 20% of Monster HP to monster", getResult5.getDescription());
        Item getResult6 = actualItemList.get(4);
        assertEquals("Deal Damage(L) to yourself, Increase ATK(MAX) for a battle", getResult6.getDescription());
        Item getResult7 = actualItemList.get(10);
        assertEquals("Raise DEF(M) for 1 battle, Raise DEF(S) permanently", getResult7.getDescription());
        assertEquals("Recover HP(S)", getResult.getDescription());
        Item getResult8 = actualItemList.get(3);
        assertEquals("Deal Damage(S) to monster, Decrease DEF(M) to monster", getResult8.getDescription());
        Item getResult9 = actualItemList.get(1);
        assertEquals("Deal MP Damage(L) to monster", getResult9.getDescription());
        Item getResult10 = actualItemList.get(13);
        assertEquals("Recover HP (L), Raise MAX HP(S) permanently", getResult10.getDescription());
        Item getResult11 = actualItemList.get(0);
        assertEquals("Deal Damage(M) to monster, Decrease ATK(L) to monster", getResult11.getDescription());
        assertEquals("Bomb Shell", getResult8.getName());
        assertEquals("Acidic Liquid", getResult11.getName());
        assertEquals("Demon Tail", getResult6.getName());
        assertEquals("Arcane Matrix", getResult9.getName());
        assertEquals("X-Ether", getResult4.getName());
        assertEquals("Soul Matrix", getResult5.getName());
        assertEquals("Soteria's Tear", getResult7.getName());
        assertEquals("X-Potion", getResult10.getName());
        assertEquals("Ares Might", getResult3.getName());
        assertEquals("Elixir", getResult2.getName());
    }

    @Test
    void testSkillIndexOf() {
        RoomServiceImpl roomServiceImpl = new RoomServiceImpl();
        assertEquals(-1, roomServiceImpl.skillIndexOf("foo"));
        roomServiceImpl.generateSkills();
        assertEquals(3, roomServiceImpl.skillIndexOf("Blizzard"));
    }

    @Test
    void testItemIndexOf() {
        RoomServiceImpl roomServiceImpl = new RoomServiceImpl();
        assertEquals(-1, roomServiceImpl.itemIndexOf("foo"));
        roomServiceImpl.generateLoots();
        assertEquals(0, roomServiceImpl.itemIndexOf("Acidic Liquid"));
    }

    @Test
    void testItemLootDropMethodWorks() {
        List<Item> itemLootTable = new ArrayList<>();
        itemLootTable.add(new AcidicLiquid());
        itemLootTable.add(new ArcaneMatrix());
        itemLootTable.add(new AresMight());
        itemLootTable.add(new BombShell());
        itemLootTable.add(new DemonTail());
        itemLootTable.add(new Elixir());
        itemLootTable.add(new Ether());
        itemLootTable.add(new HiEther());
        itemLootTable.add(new HiPotion());
        itemLootTable.add(new Potion());
        itemLootTable.add(new SoteriasTear());
        itemLootTable.add(new SoulMatrix());
        itemLootTable.add(new XEther());
        itemLootTable.add(new XPotion());

        assertNotEquals(0, roomService.getItemLoot(pandoraMonster).size());
    }

    @Test
    void testGetMonsterSKillListWorks() {
        List<Skill> skillList = new ArrayList<>();
        skillList = new ArrayList<>();
        skillList.add(new AdrenalineRush());
        skillList.add(new AncientDischarge());
        skillList.add(new BadAura());
        skillList.add(new Blizzard());
        skillList.add(new DrainStrike());
        skillList.add(new Enroot());
        skillList.add(new Frenzy());
        skillList.add(new FullminatingDarkness());
        skillList.add(new HardSlash());
        skillList.add(new HazardForm());
        skillList.add(new HeavenWrath());
        skillList.add(new Howl());
        skillList.add(new IronNeedle());
        skillList.add(new ManaSiphon());
        skillList.add(new MaterializeArmiger());
        skillList.add(new MaterializeBarrier());
        skillList.add(new MaterializeWeapon());
        skillList.add(new MechaRenew());
        skillList.add(new MendWound());
        skillList.add(new NatureBlessing());
        skillList.add(new NatureWrath());
        skillList.add(new NormalAttack());
        skillList.add(new Recharge());
        skillList.add(new RecklessFlurry());
        skillList.add(new ShatterHeart());
        skillList.add(new VenomBite());
        skillList.add(new VolcanoEruption());
        assertNotNull(roomService.getSkillList());
    }


}
