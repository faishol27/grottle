package csui.advprog.grottle.game.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advprog.grottle.game.core.JWTGenerator;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.ArmoredSoul;
import csui.advprog.grottle.game.core.ai.Landfish;
import csui.advprog.grottle.game.core.item.*;
import csui.advprog.grottle.game.core.skill.*;
import csui.advprog.grottle.game.service.RoomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RoomController.class)
class MVCRoomControllerTest {
    @Autowired
    private MockMvc mvc;

    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    private String token;

    @MockBean
    private RoomService roomService;

    static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    Map<String, Object> claim = new HashMap<>();

    @BeforeEach
    void setUp() {
        claim.put("username", "hahaha");
        claim.put("aud", "http://localhost:8080/");
        token = jwtGen.getToken(claim);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testEquipArmorMethodWorks() throws Exception{
        Map<String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        var playerMock = new Player();
        playerMock.setId(1);
        playerMock.setMaxMpStat(5);
        playerMock.setMaxMpStat(6);
        playerMock.setHpStat(2);
        playerMock.setMpStat(3);
        playerMock.setAtkStat(10);
        playerMock.setDefStat(10);
        claim.put("player", playerMock);

        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        mvc.perform(
          post("http://localhost:8080/game/equip-armor/{armorName}", "HeavyArmor")
                .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                  .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());
    }

    @Test
    void testEquipWeaponMethodWorks() throws Exception{
        Map<String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        var playerMock = new Player();
        playerMock.setId(1);
        playerMock.setMaxMpStat(5);
        playerMock.setMaxMpStat(6);
        playerMock.setHpStat(2);
        playerMock.setMpStat(3);
        playerMock.setAtkStat(10);
        playerMock.setDefStat(10);
        claim.put("player", playerMock);

        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        mvc.perform(
                post("http://localhost:8080/game/equip-weapon/{weaponName}", "Longsword")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());
    }

    @Test
    void testEncounterNormalMonsterWorks() throws Exception{
        Map<String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        var playerMock = new Player();
        playerMock.setId(1);
        playerMock.setMaxMpStat(5);
        playerMock.setMaxMpStat(6);
        playerMock.setHpStat(2);
        playerMock.setMpStat(3);
        playerMock.setAtkStat(10);
        playerMock.setDefStat(10);
        claim.put("player", playerMock);

        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        when(roomService.getMonsterEncounter()).thenReturn(new Landfish("landfish", 1, 1, 1, 1));

        mvc.perform(
                post("http://localhost:8080/game/encounter-monster")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());

    }

    @Test
    void testEncounterBossMonsterWorks() throws Exception{
        Map<String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        var playerMock = new Player();
        playerMock.setId(1);
        playerMock.setMaxMpStat(5);
        playerMock.setMaxMpStat(6);
        playerMock.setHpStat(2);
        playerMock.setMpStat(3);
        playerMock.setAtkStat(10);
        playerMock.setDefStat(10);
        claim.put("player", playerMock);

        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        when(roomService.getBossEncounter()).thenReturn(new ArmoredSoul("ArmoredSoul", 1, 1, 1, 1));

        mvc.perform(
                post("http://localhost:8080/game/encounter-boss")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());
    }

    @Test
    void testBattleMechanicAttackWorks() throws Exception{
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new AdrenalineRush());
        skillList.add(new AncientDischarge());
        skillList.add(new BadAura());
        skillList.add(new Blizzard());
        skillList.add(new DrainStrike());
        skillList.add(new Enroot());
        skillList.add(new Frenzy());
        skillList.add(new FullminatingDarkness());
        skillList.add(new HardSlash());
        skillList.add(new HazardForm());
        skillList.add(new HeavenWrath());
        skillList.add(new Howl());
        skillList.add(new IronNeedle());
        skillList.add(new ManaSiphon());
        skillList.add(new MaterializeArmiger());
        skillList.add(new MaterializeBarrier());
        skillList.add(new MaterializeWeapon());
        skillList.add(new MechaRenew());
        skillList.add(new MendWound());
        skillList.add(new NatureBlessing());
        skillList.add(new NatureWrath());
        skillList.add(new NormalAttack());
        skillList.add(new Recharge());
        skillList.add(new RecklessFlurry());
        skillList.add(new ShatterHeart());
        skillList.add(new VenomBite());
        skillList.add(new VolcanoEruption());

        List<Item> itemLootTable = new ArrayList<>();
        itemLootTable.add(new AcidicLiquid());
        itemLootTable.add(new ArcaneMatrix());
        itemLootTable.add(new AresMight());
        itemLootTable.add(new BombShell());
        itemLootTable.add(new DemonTail());
        itemLootTable.add(new Elixir());
        itemLootTable.add(new Ether());
        itemLootTable.add(new HiEther());
        itemLootTable.add(new HiPotion());
        itemLootTable.add(new Potion());
        itemLootTable.add(new SoteriasTear());
        itemLootTable.add(new SoulMatrix());
        itemLootTable.add(new XEther());
        itemLootTable.add(new XPotion());

        Map<String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        var playerMock = new Player();
        playerMock.setId(1);
        playerMock.setMaxMpStat(5);
        playerMock.setMaxMpStat(6);
        playerMock.setHpStat(2);
        playerMock.setMpStat(3);
        playerMock.setAtkStat(10);
        playerMock.setDefStat(10);
        playerMock.addItem("Potion");
        playerMock.addItem("Ether");
        claim.put("player", playerMock);

        var landfishMonster = new Landfish("Landfish", 1, 1, 1, 1);
        claim.put("basePlayer", playerMock);
        claim.put("monster", landfishMonster);
        claim.put("monsterType", landfishMonster.getClass().getName());

        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        when(roomService.getSkillList()).thenReturn(skillList);
        when(roomService.getItemList()).thenReturn(itemLootTable);

        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "Attack")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());

        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "Skill-Blizzard")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());

        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "Item-Potion")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());

        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "Item-Potion")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());

        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "Item-Ether")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());


        mvc.perform(
                post("http://localhost:8080/game/battle/{action}", "EndTurn")
                        .contentType(APPLICATION_JSON_UTF8).content(mapToJson(bodyReq))
                        .header("Authorization", token).header("Origin", "http://localhost:8080/"))
                .andExpect(status().isOk());
    }

}
