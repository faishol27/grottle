package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.item.SoulFragment;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class SoulFragmentTest {

    private SoulFragment item;
    private Creature player;
    private Creature monster;
    private Creature basePlayer;

    @BeforeEach
    void setUp() {
        item = new SoulFragment();
        player = new Player();
        basePlayer = new Player();
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        item.execute(player, monster, basePlayer);
        assertEquals(120, player.getMaxHpStat());
        assertEquals(30, player.getMaxMpStat());
        assertEquals(12, player.getAtkStat());
        assertEquals(12, player.getDefStat());

        assertEquals(120, basePlayer.getMaxHpStat());
        assertEquals(30, basePlayer.getMaxMpStat());
        assertEquals(12, basePlayer.getAtkStat());
        assertEquals(12, basePlayer.getDefStat());
    }
}