package csui.advprog.grottle.game.core.item;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Item item = new HiEther();

    @Test
    void getName() {
        assertEquals("Hi-Ether", item.getName());
    }

    @Test
    void getDescription(){
        assertEquals("Recover MP(M)", item.getDescription());
    }
}