package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class MechaRenewTest {

    private MechaRenew skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new MechaRenew();
        player = new Player();
        player.setMpStat(200); player.setMaxMpStat(1000);player.setAtkStat(100);player.setMaxHpStat(1000);
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute(){
        assertEquals(80, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(80, player.getMpStat());
        assertEquals(1000, player.getHpStat());
    }
}