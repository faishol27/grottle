package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class DemonTailTest {

    private DemonTail item;
    private Creature player;
    private Creature monster;
    private Creature basePlayer;

    @BeforeEach
    void setUp() {
        item = new DemonTail();
        player = new Player();
        basePlayer = new Player();
        monster = new OmegaMachine("monster",200,200,200,50);
    }

    @Test
    void execute() {
        player.setHpStat(2000);
        player.setDefStat(-100);
        player.setMaxMpStat(2000);
        item.execute(player, monster, basePlayer);
        assertEquals(900, player.getHpStat());
        assertEquals(120, player.getAtkStat());
    }
}