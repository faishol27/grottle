package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class LandfishTest {
    private Class<?> landfishClass;
    private Landfish landfishMonster;

    @BeforeEach
    void setUp() throws Exception {
        landfishClass = Class.forName("csui.advprog.grottle.game.core.ai.Landfish");
        landfishMonster = new Landfish("landfish", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        landfishMonster.setHpStat(999);
        landfishMonster.setMpStat(999);
        landfishMonster.recover();
        assertEquals(1000, landfishMonster.getHpStat());
        assertEquals(1000, landfishMonster.getMpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        landfishMonster.setHpStat(100);
        landfishMonster.setMpStat(100);
        landfishMonster.setTurnCount(3);
        landfishMonster.recover();
        assertEquals(100 + 3, landfishMonster.getHpStat());
        assertEquals(100 + 2 + landfishMonster.getTurnCount(), landfishMonster.getMpStat());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(landfishMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals((int) (landfishMonster.getMaxHpStat()/5.0 + 2), landfishMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        landfishMonster.setAtkStat(100);
        assertEquals((int) (100*2.3), landfishMonster.attack());
    }

    @Test
    void testRageActuallyWork() {
        landfishMonster.setHpStat(100);
        landfishMonster.setMpStat(100);
        landfishMonster.setAtkStat(100);
        landfishMonster.setRagePoint(100);

        landfishMonster.rage();
        assertEquals(0, landfishMonster.getRagePoint());
        assertEquals(100 + 10, landfishMonster.getAtkStat());
        assertEquals(100 + 3, landfishMonster.getHpStat());
        assertEquals(100 + 2, landfishMonster.getMpStat());
    }

    @Test
    void testLandfishIsNotABoss() {
        assertEquals(false, landfishMonster.isBoss());
    }

    @Test
    void testLandfishHasSkills() {
        assertNotEquals(0, landfishMonster.getSkillList().size());
        assertEquals(2, landfishMonster.getSkillList().size());
    }

    @Test
    void testLandfishHasTurnOrder() {
        assertNotEquals(0, landfishMonster.getTurnOrder().size());
        assertEquals(4, landfishMonster.getTurnOrder().size());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("Landfish is enraged! It gets increased ATK(M) and increased recovery power!", landfishMonster.getRageDescription());
    }
}
