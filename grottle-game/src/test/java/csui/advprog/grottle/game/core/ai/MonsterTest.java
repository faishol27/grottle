package csui.advprog.grottle.game.core.ai;

import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.skill.BadAura;
import csui.advprog.grottle.game.core.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MonsterTest {
    private Class<?> monsterClass;
    private Pandora pandoraMonster;
    private Player demoPlayer;

    @BeforeEach
    void setUp() throws Exception {
        monsterClass = Class.forName("csui.advprog.grottle.game.core.ai.Monster");
        demoPlayer = new Player();
        demoPlayer.setMaxHpStat(100);
        demoPlayer.setMaxMpStat(100);
        demoPlayer.setAtkStat(10);
        demoPlayer.setDefStat(10);
        demoPlayer.setHpStat(100);
        demoPlayer.setMpStat(100);
        pandoraMonster = new Pandora("Pandora", 100, 20, 5, 50);
    }

    @Test
    void testMonsterIsAbstractClass() {
        assertTrue(Modifier.isAbstract(monsterClass.getModifiers()));
    }

    @Test
    void testTakeDamageActuallyWork() {
        pandoraMonster.setHpStat(100);
        pandoraMonster.takeDamage(50);
        assertEquals(99 , pandoraMonster.getHpStat());
    }

    @Test
    void testTakeTurnActuallyWork() {
        Pandora spy = Mockito.spy(pandoraMonster);
        pandoraMonster.setHpStat(50);
        pandoraMonster.setMaxHpStat(100);
        pandoraMonster.setMpStat(50);
        pandoraMonster.setMaxMpStat(100);
        pandoraMonster.setRagePoint(0);
        pandoraMonster.setAtkStat(10);

        pandoraMonster.takeTurn(demoPlayer);

        assertEquals(50, pandoraMonster.getHpStat());
        assertEquals(50+30, pandoraMonster.getMpStat());

        pandoraMonster.setRagePoint(50);
        pandoraMonster.takeTurn(demoPlayer);

        assertEquals(51, pandoraMonster.getHpStat());
        assertEquals(50,
                pandoraMonster.getRagePoint());
    }

    @Test
    void testGetMonsterName() {
        assertEquals("Pandora", pandoraMonster.getName());
    }

    @Test
    void getSkillIndexReturnsNegativeValueIfNameIsNotFound() {
        int x = pandoraMonster.getSkillIndex("NoSkill");
        assertEquals(-1, x);
    }

    @Test
    void testSetTurnOrderMethodExist() {
        List<String> turnOrder = new ArrayList<>();
        turnOrder.add("Attack");
        pandoraMonster.setTurnOrder(turnOrder);
        assertEquals(turnOrder, pandoraMonster.getTurnOrder());
    }

    @Test
    void testSetSkillListMethodExist() {
        List<Skill> skillList = new ArrayList<>();
        Skill skill = new BadAura();
        skillList.add(skill);
        pandoraMonster.setSkillList(skillList);
        assertEquals(skillList, pandoraMonster.getSkillList());
    }

    @Test
    void testSetBossMethodExist() {
        boolean boss = false;
        pandoraMonster.setBoss(boss);
        assertEquals(boss, pandoraMonster.isBoss());
    }

}
