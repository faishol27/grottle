package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.Frenzy;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class FrenzyTest {

    private Frenzy skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new Frenzy();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",200,200,200,100);
    }

    @Test
    void execute() {
        assertEquals(81, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 2);
        assertEquals(81, player.getMpStat());
        assertEquals(104, monster.getHpStat());
        assertEquals(106,player.getAtkStat());
    }
}