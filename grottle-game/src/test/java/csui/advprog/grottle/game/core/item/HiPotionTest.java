package csui.advprog.grottle.game.core.item;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.item.HiPotion;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class HiPotionTest {

    private HiPotion item;
    private Creature player;
    private Creature monster;
    private Creature basePlayer;

    @BeforeEach
    void setUp() {
        item = new HiPotion();
        player = new Player();
        basePlayer = new Player();
        monster = new OmegaMachine("monster",200,200,200,200);
    }

    @Test
    void execute() {
        player.setMaxHpStat(1000);
        item.execute(player, monster, basePlayer);
        assertEquals(250, player.getHpStat());
    }
}