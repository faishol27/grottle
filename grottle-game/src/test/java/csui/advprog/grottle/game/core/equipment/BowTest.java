package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class BowTest {
    private Class<?> test;
    Bow bowTest = new Bow();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Bow");
    }

    @Test
    void testBowIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testBowAttackStat() {
        assertEquals(5, bowTest.getAttackStat());
    }

    @Test
    void testBowDefenseStat() {
        assertEquals(3, bowTest.getDefenseStat());
    }

    @Test
    void testBowIsAnEquipment() {
        assertTrue(bowTest instanceof Equipment);
    }

}
