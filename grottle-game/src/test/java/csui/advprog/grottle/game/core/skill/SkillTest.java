package csui.advprog.grottle.game.core.skill;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


class SkillTest {

    Skill skill;

    @BeforeEach
    void setUp() {
        skill = new ShatterHeart();
    }

    @Test
    void getName() {
        assertEquals("Shatter Heart", skill.getName());
    }

    @Test
    void getDescription() {
        assertEquals("Lower Target's DEF(L), Deal Damage(M)", skill.getDescription());
    }

    @Test
    void getMultGrowth() {
        assertEquals(0.05, skill.getMultGrowth());
    }

    @Test
    void getSkillType(){
        assertEquals("csui.advprog.grottle.game.core.skill.ShatterHeart", skill.getSkillType());
    }
}