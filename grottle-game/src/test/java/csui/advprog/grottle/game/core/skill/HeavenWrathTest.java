package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HeavenWrathTest {

    private HeavenWrath skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new HeavenWrath();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);
        monster = new OmegaMachine("monster",900,200,200,200);
    }

    @Test
    void execute(){
        assertEquals(100, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(300, monster.getHpStat());
        assertEquals(0, player.getMpStat());
    }
}