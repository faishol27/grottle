package csui.advprog.grottle.game.core;

import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(MockitoExtension.class)
class JWTGeneratorTest {
    private JWTGenerator jwtGen = JWTGenerator.getInstance();

    @Test
    void testJWTGeneratorIsSingleton() throws Exception {
        assertEquals(JWTGenerator.getInstance(), jwtGen);
    }

    @Test
    void testJWTGeneratorGetTokenCorrect() throws Exception {
        String tmpKey = System.getProperty("grottle.jwtkey", "q3t6w9z$C&F)J@vcQfTjWnZr4u7x!A%D*G-TaPdSgUkXp2s5v8y/B?s(H+MbQeTh");
        Map<String, Object> bodyClaim = new HashMap<>();
        bodyClaim.put("user", "faisholCakep");
        String tokenRet = jwtGen.getToken(bodyClaim);

        Claims claim = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(tmpKey.getBytes())).build()
                    .parseClaimsJws(tokenRet)
                    .getBody();
        assertNotEquals(null, claim);
    }

    @Test
    void testJWTGeneratorValidateTokenCorrect() throws Exception {
        Map<String, Object> bodyClaim = new HashMap<>();
        bodyClaim.put("user", "faisholCakep");
        String token = jwtGen.getToken(bodyClaim);
        Map<String, Object> bodyRet = jwtGen.getClaim(token);
        assertNotEquals(null, bodyRet);
    }

    @Test
    void testJWTGeneratorValidateTokenInvalidToken() throws Exception {
        String token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImZhaXNob2xDYWtlcCJ9.rM4GEpT0Yoo6F0krkoffvEfrr7ut_6PwkFCPMuzQSTaqa7S0zSk8empNg4veDZ2i3DS-GCYUCAoz_5o3GVNmsA";
        Map<String, Object> bodyRet = jwtGen.getClaim(token);
        assertEquals(null, bodyRet);
    }
}
