package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class DaggerTest {
    private Class<?> test;
    Dagger daggerTest = new Dagger();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Dagger");
    }

    @Test
    void testDaggerIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testDaggerAttackStat() {
        assertEquals(3, daggerTest.getAttackStat());
    }

    @Test
    void testDaggerDefenseStat() {
        assertEquals(1, daggerTest.getDefenseStat());
    }

    @Test
    void testDaggerIsAnEquipment() {
        assertTrue(daggerTest instanceof Equipment);
    }

}
