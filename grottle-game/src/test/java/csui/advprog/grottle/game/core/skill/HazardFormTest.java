package csui.advprog.grottle.game.core.skill;

import csui.advprog.grottle.game.core.Creature;
import csui.advprog.grottle.game.core.Player;
import csui.advprog.grottle.game.core.ai.OmegaMachine;
import csui.advprog.grottle.game.core.skill.HazardForm;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class HazardFormTest {

    private HazardForm skill;
    private Creature player;
    private Creature monster;

    @BeforeEach
    void setUp() {
        skill = new HazardForm();
        player = new Player();
        player.setMpStat(100); player.setMaxMpStat(1000);player.setAtkStat(100);player.setHpStat(100);
        monster = new OmegaMachine("monster",200,200,200,50);
    }

    @Test
    void execute(){
        assertEquals(82, player.getMpStat() - skill.getMpCost());
        skill.execute(player, monster, 4);
        assertEquals(4, player.getHpStat());
        assertEquals(112, player.getAtkStat());
        assertEquals(82, player.getMpStat());
    }
}