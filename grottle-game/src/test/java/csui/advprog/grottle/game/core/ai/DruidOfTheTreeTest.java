package csui.advprog.grottle.game.core.ai;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DruidOfTheTreeTest {
    private Class<?> druidOfTheTreeClass;
    private DruidOfTheTree druidOfTheTreeMonster;

    @BeforeEach
    void setUp() throws Exception {
        druidOfTheTreeClass = Class.forName("csui.advprog.grottle.game.core.ai.DruidOfTheTree");
        druidOfTheTreeMonster = new DruidOfTheTree("druidOfTheTree", 1000, 1000, 100, 100);
    }

    @Test
    void testRecoverMethodDoesNotOverflow() {
        druidOfTheTreeMonster.setHpStat(999);

        druidOfTheTreeMonster.recover();
        assertEquals(1000, druidOfTheTreeMonster.getHpStat());
    }

    @Test
    void testRecoverActuallyRecoverStat() {
        druidOfTheTreeMonster.setHpStat(100);
        druidOfTheTreeMonster.setMaxHpStat(1000);
        druidOfTheTreeMonster.setRagePoint(0);

        druidOfTheTreeMonster.recover();
        assertEquals(100 + druidOfTheTreeMonster.getAtkStat(), druidOfTheTreeMonster.getHpStat());
    }

    @Test
    void testGenerateTurnOrderActuallyWork() {
        assertNotNull(druidOfTheTreeMonster.getTurnOrder());
    }

    @Test
    void testGenerateRagePointActuallyWork() {
        assertEquals(druidOfTheTreeMonster.getAtkStat(),
                druidOfTheTreeMonster.getRagePoint());
    }

    @Test
    void testAttackActuallyWorks() {
        druidOfTheTreeMonster.setHpStat(1000);
        druidOfTheTreeMonster.setAtkStat(100);

        assertEquals(100 + 10,druidOfTheTreeMonster.attack());
    }

    @Test
    void testRageActuallyWork() {
        druidOfTheTreeMonster.setHpStat(1000);
        druidOfTheTreeMonster.setMaxHpStat(1000);
        druidOfTheTreeMonster.setRagePoint(1001);
        druidOfTheTreeMonster.setAtkStat(100);
        druidOfTheTreeMonster.setDefStat(100);

        druidOfTheTreeMonster.rage();

        assertEquals(200, druidOfTheTreeMonster.getAtkStat());
        assertEquals(0, druidOfTheTreeMonster.getDefStat());
    }

    @Test
    void testdruidOfTheTreeIsABoss() {
        assertEquals(true, druidOfTheTreeMonster.isBoss());
    }

    @Test
    void testdruidOfTheTreeHasSkills() {
        assertNotEquals(0, druidOfTheTreeMonster.getSkillList().size());
        assertEquals(6, druidOfTheTreeMonster.getSkillList().size());
    }

    @Test
    void testdruidOfTheTreeHasTurnOrder() {
        assertNotEquals(0, druidOfTheTreeMonster.getTurnOrder().size());
        assertEquals(10, druidOfTheTreeMonster.getTurnOrder().size());
    }

    @Test
    void testGetRageDescriptionReturnsCorrectString() {
        assertEquals("The Druid converts all its DEF into ATK!", druidOfTheTreeMonster.getRageDescription());
    }
}
