package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class RobeTest {
    private Class<?> test;
    Robe robeTest = new Robe();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.Robe");
    }

    @Test
    void testRobeIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testRobeHpStat() {
        assertEquals(10, robeTest.getHpStat());
    }

    @Test
    void testRobeDefenseStat() {
        assertEquals(3, robeTest.getDefenseStat());
    }

    @Test
    void testRobeIsAnEquipment() {
        assertTrue(robeTest instanceof Equipment);
    }

}
