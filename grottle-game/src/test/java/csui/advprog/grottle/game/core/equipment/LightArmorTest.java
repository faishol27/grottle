package csui.advprog.grottle.game.core.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class LightArmorTest {
    private Class<?> test;
    LightArmor lightArmorTest = new LightArmor();

    @BeforeEach
    void setUp() throws Exception {
        test = Class.forName("csui.advprog.grottle.game.core.equipment.LightArmor");
    }

    @Test
    void testLightArmorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(test.getModifiers()));
    }

    @Test
    void testLightArmorHpStat() {
        assertEquals(25, lightArmorTest.getHpStat());
    }

    @Test
    void testLightArmorDefenseStat() {
        assertEquals(5, lightArmorTest.getDefenseStat());
    }

    @Test
    void testLightArmorIsAnEquipment() {
        assertTrue(lightArmorTest instanceof Equipment);
    }

}
