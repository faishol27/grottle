CREATE TABLE IF NOT EXISTS GROTTLE.PLAYER (
    id SERIAL,
    account_id INT NOT NULL,
    currenthp INT,
    maxhp INT,
    currentmp INT,
    maxmp INT,
    attack INT,
    defense INT,
    skills TEXT,
    items TEXT,
    equipments TEXT,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES GROTTLE.ACCOUNT(id) ON UPDATE RESTRICT ON DELETE CASCADE
);