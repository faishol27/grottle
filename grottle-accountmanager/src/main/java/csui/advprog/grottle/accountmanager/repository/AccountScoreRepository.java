package csui.advprog.grottle.accountmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;

public interface AccountScoreRepository extends JpaRepository<AccountScoreModel, Object> {
    AccountScoreModel findByAccount(AccountModel acc);
}
