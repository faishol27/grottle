package csui.advprog.grottle.accountmanager.core.builder;

import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.PlayerModel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Random;
import java.util.Map;
import java.util.HashMap;

public class PlayerBuilder implements CharacterBuilder {
    private Random rand = new Random();
    private PlayerModel result;
    private ObjectMapper objMapper = new ObjectMapper();

    public PlayerBuilder() {
        this.result = new PlayerModel();
    }

    @Override
    public CharacterBuilder reset() {
        this.result = new PlayerModel();
        return this;
    }

    private int getRandomInt(int val, int add) {
        return val + rand.nextInt(add);
    }

    @Override
    public CharacterBuilder setHP(int val) {
        var hp = getRandomInt(val, 75);
        result.setCurrentHP(hp);
        result.setMaxHP(hp);
        return this;
    }

    @Override
    public CharacterBuilder setMP(int val) {
        var mp = getRandomInt(val, 50);
        result.setCurrentMP(mp);
        result.setMaxMP(mp);
        return this;
    }
    
    @Override
    public CharacterBuilder setAttack(int val) {
        var att = getRandomInt(val, 20);
        result.setAttack(att);
        return this;
    }
    
    @Override
    public CharacterBuilder setDefense(int val) {
        var def = getRandomInt(val, 20);
        result.setDefense(def);
        return this;
    }

    public CharacterBuilder setAttributes() throws JsonProcessingException {
        Map<String, Object> tmp = new HashMap<>();
        var strTmp = objMapper.writeValueAsString(tmp);
        result.setEquipments(strTmp);
        result.setItems(strTmp);
        result.setSkills(strTmp);

        return this;
    }

    public CharacterBuilder setAccount(AccountModel acc) {
        result.setAccount(acc);
        return this;
    }

    public PlayerModel getResult() {
        return this.result;
    }
}
