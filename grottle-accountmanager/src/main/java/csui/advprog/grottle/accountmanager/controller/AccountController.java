package csui.advprog.grottle.accountmanager.controller;

import csui.advprog.grottle.accountmanager.service.AccountService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/account")
public class AccountController {
    @Autowired
    private AccountService accService;

    @PostMapping(path = "/register", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> registerAccount(@RequestBody Map<String, Object> bodyReq) {
        String name = (String) bodyReq.get("name");
        String username = (String) bodyReq.get("username");
        String pwd1 = (String) bodyReq.get("password");
        String pwd2 = (String) bodyReq.get("passwordConfirm");
        if (name == null || username == null || pwd1 == null || pwd2 == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Map<String, String> registRet = accService.registerAccount(name, username, pwd1, pwd2);
        if (registRet.get("status").equals("error")) {
            return new ResponseEntity<>(registRet, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(registRet);
    }

    @PostMapping(path = "/login", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> loginAccount(@RequestBody Map<String, Object> bodyReq) {
        String username = (String) bodyReq.get("username");
        String pwd = (String) bodyReq.get("password");
        if (username == null || pwd == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Map<String, String> loginRet = accService.loginAccount(username, pwd);
        if (loginRet.get("status").equals("error")) {
            return new ResponseEntity<>(loginRet, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok()
                .header("Authorization", loginRet.get("jwt"))
                .body(loginRet);
    }
}
