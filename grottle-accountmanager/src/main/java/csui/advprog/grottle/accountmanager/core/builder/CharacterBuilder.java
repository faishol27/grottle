package csui.advprog.grottle.accountmanager.core.builder;

public interface CharacterBuilder {
    CharacterBuilder reset();
    CharacterBuilder setHP(int val);
    CharacterBuilder setMP(int val);
    CharacterBuilder setAttack(int val);
    CharacterBuilder setDefense(int val);
}
