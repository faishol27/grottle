package csui.advprog.grottle.accountmanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advprog.grottle.accountmanager.model.PlayerModel;
import javassist.NotFoundException;


public interface PlayerService {
    public PlayerModel createNewPlayer() throws JsonProcessingException;
    public PlayerModel getLastPlayer() throws NotFoundException;
    public void savePlayerModel(PlayerModel player);
    public void addScoreToAccount();
}
