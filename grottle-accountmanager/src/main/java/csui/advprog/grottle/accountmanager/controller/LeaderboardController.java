package csui.advprog.grottle.accountmanager.controller;

import csui.advprog.grottle.accountmanager.service.LeaderboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/api/v1/leaderboard")
public class LeaderboardController {

    @Autowired
    private LeaderboardService leaderboardService;

    @GetMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getLeaderboardItem() {
        return ResponseEntity.ok(leaderboardService.getLeaderboard());
    }
}
