package csui.advprog.grottle.accountmanager.service;

import csui.advprog.grottle.accountmanager.core.Leaderboard;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaderboardServiceImpl implements LeaderboardService {

    @Autowired
    private AccountScoreRepository accountScoreRepository;

    @Override
    public List<AccountScoreModel> getLeaderboard() {
        Leaderboard leaderboard;
        leaderboard = new Leaderboard(accountScoreRepository.findAll());
        return leaderboard.getLeaderboardList();
    }
}
