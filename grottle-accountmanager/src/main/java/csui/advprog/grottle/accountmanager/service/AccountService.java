package csui.advprog.grottle.accountmanager.service;

import java.util.Map;

public interface AccountService {
    public Map<String, String> registerAccount(String name, String username, String pwd1, String pwd2);
    public Map<String, String> loginAccount(String username, String pwd);
}
