package csui.advprog.grottle.accountmanager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "account_score", schema = "grottle", catalog = "grottle")
@Data
@NoArgsConstructor
public class AccountScoreModel implements Comparable<AccountScoreModel> {
    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private int id;
    
    @Column
    private int score;
    
    @OneToOne
    @JoinColumn(name="account_id", nullable = false, updatable = false)    
    private AccountModel account;

    public AccountScoreModel(AccountModel acc) {
        this.account = acc;
        this.score = 0;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int compareTo(AccountScoreModel accountScore) {
        return accountScore.getScore() >= this.getScore() ? -1 : 0;
    }
}
