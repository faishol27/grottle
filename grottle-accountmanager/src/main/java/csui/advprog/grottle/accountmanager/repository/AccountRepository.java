package csui.advprog.grottle.accountmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import csui.advprog.grottle.accountmanager.model.AccountModel;

public interface AccountRepository extends JpaRepository<AccountModel, String> {
    AccountModel findByUsername(String username);
}
