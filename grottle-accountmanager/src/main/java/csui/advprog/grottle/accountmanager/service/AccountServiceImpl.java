package csui.advprog.grottle.accountmanager.service;

import csui.advprog.grottle.accountmanager.core.JWTGenerator;
import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import csui.advprog.grottle.accountmanager.repository.AccountRepository;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;


import java.util.Map;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private static final String STT_ERR = "error";
    private static final String STT_OK = "ok";

    @Autowired
    private AccountRepository accRepo;

    @Autowired
    private AccountScoreRepository accScoreRepo;
    
    @Autowired
    private HttpServletRequest httpRequest;

    private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
    private JWTGenerator jwtGen = JWTGenerator.getInstance();

    private Map<String, String> getResponse(String stt, String message) {
        Map<String, String> response = new HashMap<>();
        response.put("status", stt);
        response.put("message", message);
        return response; 
    }

    @Override
    public Map<String, String> registerAccount(String name, String username, String pwd1, String pwd2) {
        if (!pwd1.equals(pwd2)) {
            return getResponse(STT_ERR, "Password confirmation mismatch.");
        }
        
        AccountModel oldAcc = accRepo.findByUsername(username);
        if (oldAcc != null) {
            return getResponse(STT_ERR, "Username has already been used.");
        }

        String pwdHash = bCryptEncoder.encode(pwd1);
        var newAcc = new AccountModel(name, username, pwdHash);
        accRepo.save(newAcc);

        var newAccScore = new AccountScoreModel(newAcc);
        accScoreRepo.save(newAccScore);
 
        return getResponse(STT_OK, "Successfully register new account.");
    }
    
    @Override
    public Map<String, String> loginAccount(String username, String pwd) {
        var account = accRepo.findByUsername(username);
        if (account == null) {
            return getResponse(STT_ERR, "You have entered wrong username or password.");
        }

        boolean isPassMatch = bCryptEncoder.matches(pwd, account.getPasswordHash());
        if (!isPassMatch) {
            return getResponse(STT_ERR, "You have entered wrong username or password.");
        }
        
        Map<String, Object> claim = new HashMap<>();
        claim.put("username", account.getUsername());
        claim.put("name", account.getName());
        claim.put("aud", httpRequest.getHeader("Origin"));
        String jwtToken = jwtGen.getToken(claim);

        Map<String, String> resp = new HashMap<>();
        resp.put("status", STT_OK);
        resp.put("jwt", jwtToken);

        return resp;
    }
}
