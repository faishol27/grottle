package csui.advprog.grottle.accountmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.PlayerModel;

public interface PlayerRepository extends JpaRepository<PlayerModel, Object> {
    PlayerModel findByAccount(AccountModel acc);
}
