package csui.advprog.grottle.accountmanager.service;

import javassist.NotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import csui.advprog.grottle.accountmanager.core.builder.PlayerBuilder;
import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import csui.advprog.grottle.accountmanager.model.PlayerModel;
import csui.advprog.grottle.accountmanager.repository.AccountRepository;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;
import csui.advprog.grottle.accountmanager.repository.PlayerRepository;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private AccountRepository accRepo;

    @Autowired
    private PlayerRepository playerRepo;

    @Autowired
    private AccountScoreRepository accScoreRepo;

    private PlayerBuilder playerBuilder = new PlayerBuilder();
    
    private AccountModel getUserAccount() {
        var username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return accRepo.findByUsername(username);
    }

    @Override
    public PlayerModel createNewPlayer() throws JsonProcessingException {
        var acc = this.getUserAccount();
        PlayerModel player = playerRepo.findByAccount(acc);
        if (player != null) {
            playerRepo.delete(player);
        }
        
        playerBuilder.reset()
                .setHP(5).setMP(5)
                .setAttack(5).setDefense(5);
        playerBuilder.setAccount(acc);
        playerBuilder.setAttributes();
        
        playerRepo.save(playerBuilder.getResult());
        return playerBuilder.getResult();
    }

    @Override
    public PlayerModel getLastPlayer() throws NotFoundException {
        var acc = this.getUserAccount();
        PlayerModel player = playerRepo.findByAccount(acc);
        if (player == null) {
            throw new NotFoundException("Cannot found previous game.");
        }
        return player;
    }

    @Override
    public void savePlayerModel(PlayerModel player) {
        var acc = this.getUserAccount();
        player.setAccount(acc);
        playerRepo.save(player);
    }

    @Override
    public void addScoreToAccount() {
        var acc = this.getUserAccount();
        AccountScoreModel accScore = accScoreRepo.findByAccount(acc);
        accScore.setScore(accScore.getScore() + 1);
        accScoreRepo.save(accScore);
    }
}
