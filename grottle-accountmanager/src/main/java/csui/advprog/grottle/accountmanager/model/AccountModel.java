package csui.advprog.grottle.accountmanager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account", schema = "grottle", catalog = "grottle")
@Data
@NoArgsConstructor
public class AccountModel {
    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private int id;

    @Column(length = 100)
    private String name;

    @Column(updatable = false, unique = true, length = 20)
    private String username;

    @Column
    @JsonIgnore
    private String passwordHash;
    
    public AccountModel(String name, String username, String pwdHash) {
        this.name = name;
        this.username = username;
        this.passwordHash = pwdHash;
    }

    public String getUsername() {
        return this.username;
    }
}
