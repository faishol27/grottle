package csui.advprog.grottle.accountmanager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "player", schema = "grottle", catalog = "grottle")
@Data
@NoArgsConstructor
public class PlayerModel {
    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private int id;

    @OneToOne
    @JoinColumn(name="account_id", nullable = false, updatable = false)    
    private AccountModel account;

    @Column
    private int currentHP;

    @Column
    private int maxHP;

    @Column
    private int currentMP;

    @Column
    private int maxMP;

    @Column
    private int attack;

    @Column
    private int defense;

    @Column(columnDefinition = "text")
    private String skills;

    @Column(columnDefinition = "text")
    private String items;

    @Column(columnDefinition = "text")
    private String equipments;
}
