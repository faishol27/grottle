package csui.advprog.grottle.accountmanager.controller;

import csui.advprog.grottle.accountmanager.core.JWTGenerator;
import csui.advprog.grottle.accountmanager.model.PlayerModel;
import csui.advprog.grottle.accountmanager.service.PlayerService;
import io.jsonwebtoken.JwtException;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/player")
public class PlayerController {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private HttpServletRequest httpReq;
    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    
    private String playerModelToJwt(PlayerModel player) {
        Map <String, Object> claim = new HashMap<>();
        claim.put("aud", httpReq.getHeader("Origin"));
        claim.put("player", player);
        return jwtGen.getToken(claim);
    }

    private Map<String, Object> generateBodyResponse(int respType, Object data) {
        final var HEAD_STT = "status";
        final var HEAD_MSG = "message";
        Map<String, Object> ret = new HashMap<>();
        if (respType == 0) {
            ret.put(HEAD_STT, "error");
            ret.put(HEAD_MSG, data);
        } else if (respType == 1) {
            ret.put(HEAD_STT, "ok");
            ret.put(HEAD_MSG, data);
        }else {
            ret.put(HEAD_STT, "ok");
            ret.put("data", data);
        }
        return ret;
    }

    @GetMapping(path = "/new-game", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> createNewGame() {
        var tokenPlayer = "";
        try {
            PlayerModel ret = playerService.createNewPlayer();
            tokenPlayer = playerModelToJwt(ret);
        } catch (Exception e) {
            return new ResponseEntity<>(generateBodyResponse(0, "Something went wrong."),
                HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(generateBodyResponse(2, tokenPlayer));
    }

    @GetMapping(path = "/continue", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> continueGame() {
        var tokenPlayer = "";
        try {
            PlayerModel ret = playerService.getLastPlayer();
            tokenPlayer = playerModelToJwt(ret);
        } catch (Exception e) {
            return new ResponseEntity<>(generateBodyResponse(0, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(generateBodyResponse(2, tokenPlayer));
    }

    @PostMapping(path = "/save", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> saveGame(@RequestBody Map<String, String> bodyReq) {
        String tokenPlayer = bodyReq.get("data");
        String origin = httpReq.getHeader("Origin");
        try {
            Map<String, Object> claim = jwtGen.getClaim(tokenPlayer);
            if (claim == null || !claim.get("aud").equals(origin)) {
                throw new JwtException("Invalid JWT");
            }
            PlayerModel player = new ObjectMapper().convertValue(claim.get("player"), PlayerModel.class);
            playerService.savePlayerModel(player);
            
            String statusGame = (String) claim.get("status");
            if (statusGame != null && statusGame.equals("win")) {
                playerService.addScoreToAccount();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(generateBodyResponse(0, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(generateBodyResponse(1, "Data saved successfully."));
    }
}
