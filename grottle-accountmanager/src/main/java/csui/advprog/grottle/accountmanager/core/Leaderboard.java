package csui.advprog.grottle.accountmanager.core;

import csui.advprog.grottle.accountmanager.model.AccountScoreModel;

import java.util.*;


public class Leaderboard {

    private List<AccountScoreModel> accountScoreList;

    public Leaderboard(List<AccountScoreModel> currentAccScoreList) {
        accountScoreList = currentAccScoreList;
    }

    public List<AccountScoreModel> getLeaderboardList() {
        Collections.sort(accountScoreList, Collections.reverseOrder());
        return accountScoreList;
    }
}