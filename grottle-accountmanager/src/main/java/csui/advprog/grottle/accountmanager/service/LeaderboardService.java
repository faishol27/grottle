package csui.advprog.grottle.accountmanager.service;

import csui.advprog.grottle.accountmanager.model.AccountScoreModel;

import java.util.List;

public interface LeaderboardService {

    List<AccountScoreModel> getLeaderboard();
}

