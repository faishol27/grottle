package csui.advprog.grottle.accountmanager.security;

import csui.advprog.grottle.accountmanager.core.JWTGenerator;

import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    private JWTGenerator jwtGen = JWTGenerator.getInstance();

    public JwtAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken authentication = authenticate(request);
        if (authentication == null) {
            chain.doFilter(request, response);
            return;
        }
        
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken authenticate(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String origin = request.getHeader("Origin");
        if (token == null) {
            return null;
        }
        
        Map<String, Object> claim = jwtGen.getClaim(token);
        if (claim == null || !claim.get("aud").equals(origin)) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(claim.get("username"), null, new ArrayList<>());

    }
}
