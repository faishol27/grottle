package csui.advprog.grottle.accountmanager.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import csui.advprog.grottle.accountmanager.service.AccountServiceImpl;

import java.util.HashMap;
import java.util.Map;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AccountController.class)
class AccountControllerTest {
    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private AccountServiceImpl accService;

    private String name, username, pwd;
    private Map<String, Object> bodyReq;

    @BeforeEach
    public void setUp() {
        name = "Faishol Anak Cakep";
        username = "faisholCakep";
        pwd = "IniPassword";

        bodyReq = new HashMap<>();
        bodyReq.put("name", name);
        bodyReq.put("username", username);
        bodyReq.put("password", pwd);
        bodyReq.put("passwordConfirm", pwd);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerPostRegisterCorrect() throws Exception {
        Map<String, String> resp = new HashMap<>();
        resp.put("status", "ok");
        when(accService.registerAccount(name, username, pwd, pwd)).thenReturn(resp);

        mvc.perform(post("/api/v1/account/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"));
    }

    @Test
    void testControllerPostRegisterError() throws Exception {
        Map<String, String> resp = new HashMap<>();
        resp.put("status", "error");
        when(accService.registerAccount(name, username, pwd, pwd)).thenReturn(resp);

        mvc.perform(post("/api/v1/account/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isBadRequest())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("error"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"name", "username", "password", "passwordConfirm"})
    void testControllerPostRegisterMissingField(String arg) throws Exception {
        bodyReq.remove(arg);
        mvc.perform(post("/api/v1/account/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isBadRequest());
    }

    @ParameterizedTest
    @ValueSource(strings = {"username", "password"})
    void testControllerPostLoginMissingField(String arg) throws Exception {
        bodyReq.remove(arg);
        mvc.perform(post("/api/v1/account/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void testControllerPostLoginCorrect() throws Exception {
        Map<String, String> resp = new HashMap<>();
        resp.put("status", "ok");
        resp.put("jwt", "ini.j.wt");
        when(accService.loginAccount(username, pwd)).thenReturn(resp);

        mvc.perform(post("/api/v1/account/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isOk())
            .andExpect(header().exists("Authorization"))
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"));
    }

    @Test
    void testControllerPostLoginIncorrectCreds() throws Exception {
        Map<String, String> resp = new HashMap<>();
        resp.put("status", "error");
        when(accService.loginAccount(username, pwd)).thenReturn(resp);

        mvc.perform(post("/api/v1/account/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
            .andExpect(status().isBadRequest())
            .andExpect(header().doesNotExist("Authorization"))
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("error"));
    }
}
