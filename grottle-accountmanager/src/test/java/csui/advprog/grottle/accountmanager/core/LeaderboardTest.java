package csui.advprog.grottle.accountmanager.core;
import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

class LeaderboardTest {
    private AccountScoreModel accountScoreTest;

    private Leaderboard leaderboard;

    private AccountModel acc1;

    private AccountModel acc2;

    private AccountScoreModel accScore1;

    private AccountScoreModel accScore2;

    @BeforeEach
    void init() {
        acc1 = new AccountModel("name1", "username1","password1");
        acc2 = new AccountModel("name2", "username2","password2");
        accScore1 = new AccountScoreModel(acc1);
        accScore2 = new AccountScoreModel(acc2);
        accScore1.setScore(10);
        List<AccountScoreModel> leaderboardTest = new ArrayList<>();
        leaderboardTest.add(accScore2);
        leaderboardTest.add(accScore1);
        leaderboard = new Leaderboard(leaderboardTest);
    }

    @Test
    void testIfLeaderboardCorrectlySorted() {
        assertEquals(accScore1, leaderboard.getLeaderboardList().get(0));
        assertEquals(accScore2, leaderboard.getLeaderboardList().get(1));
    }
}
