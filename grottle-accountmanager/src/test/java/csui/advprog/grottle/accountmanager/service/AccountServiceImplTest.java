package csui.advprog.grottle.accountmanager.service;

import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.repository.AccountRepository;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {
    @Mock
    private AccountRepository accRepo;

    @Mock
    private AccountScoreRepository accScoreRepo;
    
    @Mock
    private HttpServletRequest httpReqMock;

    @InjectMocks
    private AccountServiceImpl accService;

    private AccountModel mockAcc;

    private final String name = "The Grottle";
    private final String username = "grottle";
    private final String pwd = "1234";
    private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();

    @BeforeEach
    public void setUp() {
        String pwdHash = bCryptEncoder.encode(pwd);
        mockAcc = new AccountModel(name, username, pwdHash);
        ReflectionTestUtils.setField(accService, "httpRequest", httpReqMock);
    }

    @Test
    void testRegisterAccountUsernameDuplicate() throws Exception {
        when(accRepo.findByUsername(username)).thenReturn(mockAcc);
        Map<String, String> testRes = accService.registerAccount(name, username, pwd, pwd);
        assertEquals("error", testRes.get("status"));
        assertEquals("Username has already been used.", testRes.get("message"));
    }

    @Test
    void testRegisterAccountPasswordMissmatch() throws Exception {
        Map<String, String> testRes = accService.registerAccount(name, username, pwd, "BEDA");
        assertEquals("error", testRes.get("status"));
        assertEquals("Password confirmation mismatch.", testRes.get("message"));
    }

    @Test
    void testRegisterAccountCorrect() throws Exception {
        Map<String, String> testRes = accService.registerAccount(name, username, pwd, pwd);
        assertEquals("ok", testRes.get("status"));
        assertEquals("Successfully register new account.", testRes.get("message"));
    }

    @Test
    void testLoginAccountCorrect() throws Exception {
        when(accRepo.findByUsername(username)).thenReturn(mockAcc);
        when(httpReqMock.getHeader("Origin")).thenReturn("home");
        Map<String, String> testRes = accService.loginAccount(username, pwd);
        assertEquals("ok", testRes.get("status"));
        assertNotEquals(null, testRes.get("jwt"));
    }
    
    @Test
    void testLoginAccountUsernameNotFound() throws Exception {
        Map<String, String> testRes = accService.loginAccount(username, pwd);
        assertEquals("error", testRes.get("status"));
        assertEquals("You have entered wrong username or password.", testRes.get("message"));
    }

    @Test
    void testLoginAccountPasswordWrong() throws Exception {
        when(accRepo.findByUsername(username)).thenReturn(mockAcc);
        Map<String, String> testRes = accService.loginAccount(username, "wohohoho");
        assertEquals("error", testRes.get("status"));
        assertEquals("You have entered wrong username or password.", testRes.get("message"));
    }
}
