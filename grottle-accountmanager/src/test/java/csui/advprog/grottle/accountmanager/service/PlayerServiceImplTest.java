package csui.advprog.grottle.accountmanager.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import csui.advprog.grottle.accountmanager.model.PlayerModel;
import csui.advprog.grottle.accountmanager.repository.AccountRepository;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;
import csui.advprog.grottle.accountmanager.repository.PlayerRepository;
import javassist.NotFoundException;

@ExtendWith(MockitoExtension.class)
class PlayerServiceImplTest {
    @Mock
    private AccountRepository accRepo;
    @Mock
    private PlayerRepository playerRepo;
    @Mock
    private AccountScoreRepository accScoreRepo;

    @InjectMocks
    private PlayerServiceImpl playerService;

    private AccountModel accMock = new AccountModel("a", "b", "c");
    private PlayerModel playerMock = new PlayerModel();
    private AccountScoreModel accScoreMock = new AccountScoreModel(accMock);

    static PlayerModel[] argPlayer() {
        return new PlayerModel[] {null, new PlayerModel()};
    }

    @BeforeEach
    void setUp() {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("a", null, new ArrayList<>()); 
        SecurityContextHolder.getContext().setAuthentication(auth);
        lenient().when(accRepo.findByUsername(anyString())).thenReturn(accMock);
    }

    @ParameterizedTest
    @MethodSource("argPlayer")
    void testCreateNewPlayerCorrect(PlayerModel obj) throws Exception {
        when(playerRepo.findByAccount(accMock)).thenReturn(obj);
        PlayerModel ret = playerService.createNewPlayer();
        assertNotNull(ret);
    }

    @Test
    void testGetLastPlayerCorrect() throws Exception {
        when(playerRepo.findByAccount(accMock)).thenReturn(playerMock);
        PlayerModel ret = playerService.getLastPlayer();
        assertEquals(playerMock, ret);
    }

    @Test
    void testGetLastPlayerNoPrevGame() throws Exception {
        Exception exception = assertThrows(NotFoundException.class, () -> {
            playerService.getLastPlayer();
        });
        String expectedMessage = "Cannot found previous game.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testSavePLayerModelCorrect() throws Exception {
        ReflectionTestUtils.setField(playerService, "playerRepo", playerRepo);
        playerService.savePlayerModel(playerMock);
        verify(playerRepo, times(1)).save(playerMock);
    }

    @Test
    void testAddScoreToAccountCorrect() throws Exception {
        ReflectionTestUtils.setField(playerService, "accScoreRepo", accScoreRepo);
        when(accScoreRepo.findByAccount(accMock)).thenReturn(accScoreMock);
        playerService.addScoreToAccount();
        verify(accScoreRepo, times(1)).save(accScoreMock);
    }
}
