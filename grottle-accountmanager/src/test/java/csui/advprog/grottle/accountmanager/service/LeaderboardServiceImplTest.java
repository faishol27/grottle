package csui.advprog.grottle.accountmanager.service;
import csui.advprog.grottle.accountmanager.model.AccountModel;
import csui.advprog.grottle.accountmanager.model.AccountScoreModel;
import csui.advprog.grottle.accountmanager.repository.AccountScoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class LeaderboardServiceImplTest {

    @Mock
    private AccountScoreRepository accountScoreRepository;

    @InjectMocks
    private LeaderboardServiceImpl leaderboardService;

    private AccountScoreModel accountScoreTest;

    @BeforeEach
    public void setUp() {
        accountScoreTest = new AccountScoreModel(new AccountModel("name", "username","password123"));
        accountScoreRepository.save(accountScoreTest);
    }

}
