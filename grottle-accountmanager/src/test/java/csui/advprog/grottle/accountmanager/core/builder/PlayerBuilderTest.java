package csui.advprog.grottle.accountmanager.core.builder;

import csui.advprog.grottle.accountmanager.model.PlayerModel;
import csui.advprog.grottle.accountmanager.model.AccountModel;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.fields.FieldSet;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(MockitoExtension.class)
class PlayerBuilderTest {
    private final int MOCK_VAL = 50;

    private PlayerBuilder builder = new PlayerBuilder();
    
    @Mock
    private PlayerModel player = new PlayerModel();
    
    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(builder, "result", player);        
    }

    @Test
    void testPlayerBuilderSetHPCorrect() throws Exception {
        builder.setHP(MOCK_VAL);
        verify(player, times(1)).setCurrentHP(anyInt());
        verify(player, times(1)).setMaxHP(anyInt());
    }

    @Test
    void testPlayerBuilderSetMPCorrect() throws Exception {
        builder.setMP(MOCK_VAL);
        verify(player, times(1)).setCurrentMP(anyInt());
        verify(player, times(1)).setMaxMP(anyInt());
    }

    @Test
    void testPlayerBuilderSetAttackCorrect() throws Exception {
        builder.setAttack(MOCK_VAL);
        verify(player, times(1)).setAttack(anyInt());
    }

    @Test
    void testPlayerBuilderSetDefenseCorrect() throws Exception {
        builder.setDefense(MOCK_VAL);
        verify(player, times(1)).setDefense(anyInt());
    }

    @Test
    void testPlayerBuilderGetResultCorrect() throws Exception {
        PlayerModel ret = builder.getResult();
        assertNotEquals(null, ret);
    }

    @Test
    void testPlayerBuilderResetCorrect() throws Exception {
        PlayerModel first = builder.getResult();
        builder.reset();
        PlayerModel second = builder.getResult();
        assertNotEquals(first, second);
    }

    @Test
    void testPlayerBuilderSetAccountCorrect() throws Exception {
        AccountModel mockAcc = new AccountModel("a", "b", "c");
        builder.setAccount(mockAcc);
        verify(player, times(1)).setAccount(mockAcc);
    }

    @Test
    void testPlayerBuilderSetAttributesCorrect() throws Exception {
        builder.setAttributes();
        verify(player, times(1)).setEquipments(anyString());
        verify(player, times(1)).setItems(anyString());
        verify(player, times(1)).setSkills(anyString());

    }
}
