package csui.advprog.grottle.accountmanager.controller;

import csui.advprog.grottle.accountmanager.core.JWTGenerator;
import csui.advprog.grottle.accountmanager.service.LeaderboardServiceImpl;
import csui.advprog.grottle.accountmanager.service.PlayerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LeaderboardController.class)
class LeaderboardControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LeaderboardServiceImpl leaderboardService;

    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    private String token;

    @BeforeEach
    void setUp() throws Exception {
        Map<String, Object> claim = new HashMap<>();
        claim.put("username", "test");
        claim.put("aud", "http://localhost:8080/");
        token = jwtGen.getToken(claim);
    }

    @Test
    void testLeaderboardControllerReturnsOkStatus() throws Exception {
        mvc.perform(get("/api/v1/leaderboard/")).andExpect(status().isOk());
    }
}
