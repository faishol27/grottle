package csui.advprog.grottle.accountmanager.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import csui.advprog.grottle.accountmanager.core.JWTGenerator;
import csui.advprog.grottle.accountmanager.core.builder.PlayerBuilder;
import csui.advprog.grottle.accountmanager.model.PlayerModel;
import csui.advprog.grottle.accountmanager.service.PlayerServiceImpl;
import javassist.NotFoundException;

@WebMvcTest(PlayerController.class)
class PlayerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PlayerServiceImpl playerService;

    private PlayerModel playerMock;
    private JWTGenerator jwtGen = JWTGenerator.getInstance();
    private String token;

    @BeforeEach
    void setUp() throws Exception {
        Map<String, Object> claim = new HashMap<>();
        claim.put("username", "hahaha");
        claim.put("aud", "http://localhost:8080/");
        token = jwtGen.getToken(claim);

        PlayerBuilder builder = new PlayerBuilder();
        builder.reset()
                .setHP(5).setMP(5)
                .setAttack(5).setDefense(5);
        builder.setAttributes();
        playerMock = builder.getResult();
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testNewGameNoToken() throws Exception {
        mvc.perform(get("/api/v1/player/new-game"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testNewGameInvalidToken() throws Exception {
        mvc.perform(get("/api/v1/player/new-game").header("Authorization", "jwt.palsu"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testNewGameMissmatchOrigin() throws Exception {
        mvc.perform(get("/api/v1/player/new-game")
                    .header("Authorization", token).header("Origin", "tidak sama"))
            .andExpect(status().isForbidden());
    }

    @Test
    void testNewGameCorrect() throws Exception {
        when(playerService.createNewPlayer()).thenReturn(playerMock);

        mvc.perform(get("/api/v1/player/new-game")
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"))
            .andExpect(jsonPath("$.data").exists());
    }

    @Test
    void testNewGameThrowException() throws Exception {
        when(playerService.createNewPlayer()).thenThrow(JsonProcessingException.class);
        
        mvc.perform(get("/api/v1/player/new-game")
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isInternalServerError())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("error"))
            .andExpect(jsonPath("$.message").exists());
    }

    @Test
    void testGetLastPlayerCorrect() throws Exception {
        when(playerService.getLastPlayer()).thenReturn(playerMock);

        mvc.perform(get("/api/v1/player/continue")
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"))
            .andExpect(jsonPath("$.data").exists());
    }

    @Test
    void testGetLastPlayerNoPrevGame() throws Exception {
        when(playerService.getLastPlayer()).thenThrow(new NotFoundException("hahaha error kan"));
        
        mvc.perform(get("/api/v1/player/continue")
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isBadRequest())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("error"))
            .andExpect(jsonPath("$.message").exists());
    }

    @Test
    void testSaveGameInvalidData() throws Exception {
        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", "invalid.j.wt");

        mvc.perform(post("/api/v1/player/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq))
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isBadRequest())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("error"))
            .andExpect(jsonPath("$.message").exists());
    }

    @Test
    void testSaveGameCorrect() throws Exception {
        Map <String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        claim.put("player", playerMock);
        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);

        mvc.perform(post("/api/v1/player/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq))
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"))
            .andExpect(jsonPath("$.message").exists());
        verify(playerService, times(1)).savePlayerModel(any(PlayerModel.class));
    }

    @Test
    void testSaveGameCorrectStatusWin() throws Exception {
        Map <String, Object> claim = new HashMap<>();
        claim.put("aud", "http://localhost:8080/");
        claim.put("player", playerMock);
        claim.put("status", "win");
        String playerToken = jwtGen.getToken(claim);
        assertNotNull(playerToken);

        Map<String, String> bodyReq = new HashMap<>();
        bodyReq.put("data", playerToken);
        
        mvc.perform(post("/api/v1/player/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq))
                    .header("Authorization", token).header("Origin", "http://localhost:8080/"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("ok"))
            .andExpect(jsonPath("$.message").exists());
        verify(playerService, times(1)).savePlayerModel(any(PlayerModel.class));
        verify(playerService, times(1)).addScoreToAccount();
    }
}
